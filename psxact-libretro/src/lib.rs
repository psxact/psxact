use core::slice;

use libretro_rs::*;

use psxact::{
  cdrom::core::Disc,
  console::{Console, InputParams},
  input::host_device::HostDevice,
};

struct Emulator {
  console: Console,
  disc: Disc,
  width: u32,
  height: u32,
}

impl Emulator {
  pub fn input(handle: &RetroRuntime) -> InputParams {
    InputParams {
      device1: HostDevice {
        dpad_down: handle.is_joypad_button_pressed(0, RetroJoypadButton::Down).into(),
        dpad_left: handle.is_joypad_button_pressed(0, RetroJoypadButton::Left).into(),
        dpad_right: handle.is_joypad_button_pressed(0, RetroJoypadButton::Right).into(),
        dpad_up: handle.is_joypad_button_pressed(0, RetroJoypadButton::Up).into(),
        left_back_shoulder: handle.is_joypad_button_pressed(0, RetroJoypadButton::L2).into(),
        left_front_shoulder: handle.is_joypad_button_pressed(0, RetroJoypadButton::L1).into(),
        right_back_shoulder: handle.is_joypad_button_pressed(0, RetroJoypadButton::R2).into(),
        right_front_shoulder: handle.is_joypad_button_pressed(0, RetroJoypadButton::R1).into(),
        select: handle.is_joypad_button_pressed(0, RetroJoypadButton::Select).into(),
        start: handle.is_joypad_button_pressed(0, RetroJoypadButton::Start).into(),
        button_0: handle.is_joypad_button_pressed(0, RetroJoypadButton::B).into(),
        button_1: handle.is_joypad_button_pressed(0, RetroJoypadButton::Y).into(),
        button_2: handle.is_joypad_button_pressed(0, RetroJoypadButton::A).into(),
        button_3: handle.is_joypad_button_pressed(0, RetroJoypadButton::X).into(),
      },
      device2: HostDevice::default(),
    }
  }
}

impl RetroCore for Emulator {
  fn init(env: &RetroEnvironment) -> Emulator {
    let core_assets_path = env.get_system_directory().unwrap();
    let bios_path = format!("{}/psxact/bios.bin", core_assets_path);

    Emulator {
      console: Console::new(&bios_path),
      disc: Disc::None,
      width: 640,
      height: 480,
    }
  }

  fn get_system_info() -> RetroSystemInfo {
    RetroSystemInfo::new("psxact", env!("CARGO_PKG_VERSION"))
      .with_block_extract()
      .with_need_full_path()
      .with_valid_extensions(&["bin"])
  }

  fn load_game(&mut self, _: &RetroEnvironment, game: RetroGame) -> RetroLoadGameResult {
    self.disc = match game {
      RetroGame::None { .. } => Disc::None,
      RetroGame::Data { .. } => {
        println!("loading a game from binary data isn't supported");
        return RetroLoadGameResult::Failure;
      }
      RetroGame::Path { path, .. } => Disc::disc(path).unwrap_or(Disc::None),
    };

    RetroLoadGameResult::Success {
      region: RetroRegion::NTSC,
      audio: RetroAudioInfo::new(44_100.0),
      video: RetroVideoInfo::new(60.0, 640, 480)
        .with_aspect_ratio(4.0 / 3.0)
        .with_pixel_format(RetroPixelFormat::XRGB8888),
    }
  }

  fn unload_game(&mut self, _: &RetroEnvironment) {
    self.disc = Disc::None;
  }

  fn run(&mut self, env: &RetroEnvironment, handle: &RetroRuntime) {
    let input = Self::input(handle);
    let output = self.console.run_for_one_frame(&mut self.disc, &input);

    if self.width != output.video.width || self.height != output.video.height {
      self.width = output.video.width;
      self.height = output.video.height;

      env.set_geometry(RetroGameGeometry::new(output.video.width, output.video.height, 4.0 / 3.0));
    }

    handle.upload_audio_frame(output.audio.buffer);
    handle.upload_video_frame(
      convert_video_buffer(output.video.buffer),
      output.video.width,
      output.video.height,
      output.video.pitch,
    );
  }

  fn reset(&mut self, _: &RetroEnvironment) {}
}

fn convert_video_buffer(buf: &[u32]) -> &[u8] {
  // TODO: This probably isn't portable.
  unsafe {
    let data = buf.as_ptr() as *const u8;
    let len = buf.len() * 4;
    slice::from_raw_parts(data, len)
  }
}

libretro_core!(Emulator);
