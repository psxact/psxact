use std::io::Result;

use crate::{
  cdrom::{
    core::{CdromCore, Disc},
    xa_adpcm_decoder::XaAdpcmDecoder,
  },
  cpu::{core::CpuCore, dma::DmaCore},
  exp::{exp1::*, exp2::*, exp3::*},
  gpu::core::GpuCore,
  input::{core::InputCore, host_device::HostDevice},
  mdec::MdecCore,
  memory::{bios::Bios, mem_ctl::MemCtl, wram::Wram, MemoryBase},
  spu::core::SpuCore,
  timer::TimerCore,
  util::{
    events::{Event, EventCollector},
    range,
  },
  AddressWidth, InterruptType, IO,
};

#[derive(Default)]
pub struct InputParams {
  pub device1: HostDevice,
  pub device2: HostDevice,
}

pub struct OutputParams<'a> {
  pub audio: OutputParamsAudio<'a>,
  pub video: OutputParamsVideo<'a>,
}

pub struct OutputParamsAudio<'a> {
  pub buffer: &'a [i16],
}

pub struct OutputParamsVideo<'a> {
  pub buffer: &'a [u32],
  pub width: u32,
  pub height: u32,
  pub pitch: usize,
}

pub struct IOMap {
  bios: Bios,
  cdrom: CdromCore,
  dma: DmaCore,
  exp1: Expansion1,
  exp2: Expansion2,
  exp3: Expansion3,
  gpu: GpuCore,
  input: InputCore,
  mdec: MdecCore,
  mem_ctl: MemCtl,
  spu: SpuCore,
  timer: TimerCore,
  wram: Wram,
}

impl IOMap {
  pub fn new(bios_path: &str) -> IOMap {
    IOMap {
      bios: Bios::new(bios_path).unwrap(),
      cdrom: CdromCore::new(),
      dma: DmaCore::new(),
      exp1: Expansion1,
      exp2: Expansion2,
      exp3: Expansion3,
      gpu: GpuCore::new(),
      input: InputCore::new(),
      mdec: MdecCore::new(),
      mem_ctl: MemCtl::new(),
      spu: SpuCore::new(),
      timer: TimerCore::new(),
      wram: Wram::new(),
    }
  }

  pub fn dma(&mut self) -> i32 {
    let mut amount = 0;
    amount += self.dma.tick_channel(0, &mut self.wram, &mut self.mdec);
    amount += self.dma.tick_channel(1, &mut self.wram, &mut self.mdec);
    amount += self.dma.tick_channel(2, &mut self.wram, &mut self.gpu);
    amount += self.dma.tick_channel(3, &mut self.wram, &mut self.cdrom);
    amount += self.dma.tick_channel(4, &mut self.wram, &mut self.spu);
    // TODO: PIO (5)
    amount += self.dma.tick_channel_otc(&mut self.wram);

    amount
  }

  pub fn decode(&mut self, address: u32) -> &mut dyn IO {
    // if (between(0x1F80_1800, 0x1F80_1803)) {
    //   if (load_exe_pending) {
    //     load_exe_pending = false;
    //     load_exe(opt->get_game_file());
    //   }

    //   return &cdrom_addressable;
    // }

    if range::between(address, 0x1FC0_0000, 0x1FC7_FFFF) {
      return &mut self.bios;
    }

    if range::between(address, 0x0000_0000, 0x007F_FFFF) {
      return &mut self.wram;
    }

    if range::between(address, 0x1F80_1040, 0x1F80_104F) {
      return &mut self.input;
    }

    if range::between(address, 0x1F80_1080, 0x1F80_10FF) {
      return &mut self.dma;
    }

    if range::between(address, 0x1F80_1100, 0x1F80_113F) {
      return &mut self.timer;
    }

    if range::between(address, 0x1F80_1800, 0x1F80_1803) {
      return &mut self.cdrom;
    }

    if range::between(address, 0x1F80_1810, 0x1F80_1817) {
      return &mut self.gpu;
    }

    if range::between(address, 0x1F80_1820, 0x1F80_1827) {
      return &mut self.mdec;
    }

    if range::between(address, 0x1F80_1C00, 0x1F80_1FFF) {
      return &mut self.spu;
    }

    if range::between(address, 0x1F00_0000, 0x1F7F_FFFF) {
      return &mut self.exp1;
    }

    if range::between(address, 0x1F80_2000, 0x1F80_2FFF) {
      return &mut self.exp2;
    }

    if range::between(address, 0x1FA0_0000, 0x1FBF_FFFF) {
      return &mut self.exp3;
    }

    &mut self.mem_ctl
  }
}

impl IO for IOMap {
  fn io_read(&mut self, width: AddressWidth, address: u32) -> u32 {
    self.decode(address).io_read(width, address)
  }

  fn io_write(&mut self, width: AddressWidth, address: u32, val: u32) {
    self.decode(address).io_write(width, address, val)
  }
}

pub struct Console {
  cpu: CpuCore,
  map: IOMap,
  events: EventCollector,
  xa_adpcm: XaAdpcmDecoder,
}

impl Console {
  pub fn new(bios_path: &str) -> Console {
    Console {
      cpu: CpuCore::new(),
      map: IOMap::new(bios_path),
      events: EventCollector::new(),
      xa_adpcm: XaAdpcmDecoder::new(),
    }
  }

  pub fn get_audio_params(&self) -> OutputParamsAudio {
    OutputParamsAudio {
      buffer: self.map.spu.get_sample_buffer(),
    }
  }

  pub fn get_video_params(&self) -> OutputParamsVideo {
    OutputParamsVideo {
      buffer: self.map.gpu.get_video_buffer(),
      width: self.map.gpu.get_h_resolution() as u32,
      height: 480,
      pitch: 640 * 4,
    }
  }

  pub fn run_for_one_frame(&mut self, disc: &mut Disc, i: &InputParams) -> OutputParams {
    self.map.input.latch(&i.device1, &i.device2);
    self.map.gpu.start_frame();
    self.map.spu.start_frame();

    let mut running = true;

    while running {
      let amount = self.cpu.tick(&mut self.map) + self.map.dma();
      self.map.dma.irq_edge(&mut self.events);

      self.map.spu.tick(amount, &mut self.xa_adpcm);
      self.map.timer.tick(amount);
      self.map.cdrom.tick(amount, &mut self.events, disc, &mut self.xa_adpcm);
      self.map.input.tick(amount, &mut self.events);

      running = !self.map.gpu.tick(amount, &mut self.events);

      if self.events.len() != 0 {
        for evt in self.events.events() {
          match evt {
            Event::IrqAck(int) => self.cpu.ack(*int),
            Event::IrqSet(InterruptType::Input) => self.cpu.interrupt(InterruptType::Input),
            Event::IrqSet(int) => self.cpu.irq(*int),
            Event::HblEnter => self.map.timer.enter_hblank(),
            Event::HblLeave => self.map.timer.leave_hblank(),
            Event::VblEnter => {
              self.cpu.interrupt(InterruptType::VBlank);
              self.map.timer.enter_vblank()
            }
            Event::VblLeave => self.map.timer.leave_vblank(),
          }
        }
      }

      self.events.clear()
    }

    OutputParams {
      audio: self.get_audio_params(),
      video: self.get_video_params(),
    }
  }

  pub fn load_exe(&mut self, game_file: &str) -> Result<()> {
    // load the exe into ram
    let blob = MemoryBase::rom(game_file)?;

    let sp = blob.read_word(0x30) + blob.read_word(0x34);

    self.cpu.set_pc(blob.read_word(0x10));
    self.cpu.set_register(4, 1);
    self.cpu.set_register(5, 0);
    self.cpu.set_register(28, blob.read_word(0x14));
    self.cpu.set_register(29, sp);
    self.cpu.set_register(30, sp);

    let text_start = blob.read_word(0x18);
    let text_count = blob.read_word(0x1C);

    println!("Loading executable into WRAM..");
    println!("  PC: 0x{:08x}", self.cpu.get_pc());
    println!("  GP: 0x{:08x}", self.cpu.get_register(28));
    println!("  SP: 0x{:08x}", self.cpu.get_register(29));
    println!("  FP: 0x{:08x}", self.cpu.get_register(30));
    println!("  Text Start: 0x{:08x}", text_start);
    println!("  Text Count: 0x{:08x}", text_count);

    for i in 0..text_count {
      self
        .map
        .wram
        .io_write(AddressWidth::Byte, text_start + i, blob.read_byte(0x800 + i) as u32);
    }

    Ok(())
  }
}
