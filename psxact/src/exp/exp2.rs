use crate::{timing::timing_add_cpu_time, AddressWidth, IO};

pub struct Expansion2;

fn add_cpu_time(width: &AddressWidth) {
  match width {
    AddressWidth::Byte => timing_add_cpu_time(12),
    AddressWidth::Half => timing_add_cpu_time(27),
    AddressWidth::Word => timing_add_cpu_time(57),
  }
}

impl IO for Expansion2 {
  fn io_read(&mut self, width: AddressWidth, address: u32) -> u32 {
    add_cpu_time(&width);

    if let AddressWidth::Byte = width {
      if address == 0x1F80_2021 {
        return 0x04;
      }
    }

    panic!("")
  }

  fn io_write(&mut self, width: AddressWidth, address: u32, data: u32) {
    add_cpu_time(&width);

    if let AddressWidth::Byte = width {
      match address {
        0x1F80_2020 => return,
        0x1F80_2021 => return,
        0x1F80_2022 => return,
        0x1F80_2023 => {
          print!("{}", (data as u8) as char);
          return;
        }
        0x1F80_2024 => return,
        0x1F80_2025 => return,
        0x1F80_202A => return,
        0x1F80_202D => return,
        0x1F80_202E => return,
        0x1F80_2041 => return,
        _ => (),
      }
    }

    panic!("")
  }
}
