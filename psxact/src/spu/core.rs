use crate::{
  cdrom::xa_adpcm_decoder::XaAdpcmDecoder,
  spu::sound_ram::SoundRamAddress,
  timing::{timing_add_cpu_time, SPU_DIVIDER},
  AddressWidth, Comms, IO,
};

use super::{sound_ram::SoundRam, voice::Voice};

pub enum SpuRegister {
  KonLo,
  KonHi,
  KoffLo,
  KoffHi,
  PmonLo,
  PmonHi,
  EndxLo,
  EndxHi,
  RamAddrIrq,
  RamAddr,
  RamData,
  Control,
  Status,
  CdVolumeLeft,
  CdVolumeRight,
  Unknown(u32),
}

impl From<u32> for SpuRegister {
  fn from(address: u32) -> Self {
    match address {
      0x1F80_1D88 => Self::KonLo,
      0x1F80_1D8A => Self::KonHi,
      0x1F80_1D8C => Self::KoffLo,
      0x1F80_1D8E => Self::KoffHi,
      0x1F80_1D90 => Self::PmonLo,
      0x1F80_1D92 => Self::PmonHi,
      0x1F80_1D9C => Self::EndxLo,
      0x1F80_1D9A => Self::EndxHi,
      0x1F80_1DA4 => Self::RamAddrIrq,
      0x1F80_1DA6 => Self::RamAddr,
      0x1F80_1DA8 => Self::RamData,
      0x1F80_1DAA => Self::Control,
      0x1F80_1DAE => Self::Status,
      0x1F80_1DB0 => Self::CdVolumeLeft,
      0x1F80_1DB2 => Self::CdVolumeRight,
      address => Self::Unknown(address),
    }
  }
}

impl Into<u32> for SpuRegister {
  fn into(self) -> u32 {
    match self {
      Self::KonLo => 0x1F80_1D88,
      Self::KonHi => 0x1F80_1D8A,
      Self::KoffLo => 0x1F80_1D8C,
      Self::KoffHi => 0x1F80_1D8E,
      Self::PmonLo => 0x1F80_1D90,
      Self::PmonHi => 0x1F80_1D92,
      Self::EndxLo => 0x1F80_1D9C,
      Self::EndxHi => 0x1F80_1D9A,
      Self::RamAddrIrq => 0x1F80_1DA4,
      Self::RamAddr => 0x1F80_1DA6,
      Self::RamData => 0x1F80_1DA8,
      Self::Control => 0x1F80_1DAA,
      Self::Status => 0x1F80_1DAE,
      Self::CdVolumeLeft => 0x1F80_1DB0,
      Self::CdVolumeRight => 0x1F80_1DB2,
      Self::Unknown(address) => address,
    }
  }
}

pub struct SpuCore {
  registers: [u16; 512],

  cd_volume_left: i16,
  cd_volume_right: i16,

  ram: SoundRam,
  ram_address: u32,
  ram_address_irq: u32,

  capture_address: u32,

  voices: [Voice; 24],
  key_on: u32,
  key_off: u32,
  pmon: u32,
  endx: u32,

  prescaler: i32,

  samples: Vec<i16>,
}

impl SpuCore {
  pub fn new() -> SpuCore {
    SpuCore {
      registers: [0; 512],
      cd_volume_left: 0,
      cd_volume_right: 0,
      ram: SoundRam::new(),
      ram_address: 0,
      ram_address_irq: 0,
      capture_address: 0,
      voices: [
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
        Voice::new(),
      ],
      key_on: 0,
      key_off: 0,
      pmon: 0,
      endx: 0,
      prescaler: 0,
      samples: Vec::with_capacity(4096),
    }
  }

  pub fn get_sample_buffer(&self) -> &[i16] {
    &self.samples
  }

  pub fn start_frame(&mut self) {
    self.samples.clear()
  }

  pub fn get_register<T: Into<u32>>(&self, reg: T) -> u16 {
    self.registers[reg.into() as usize - 0x1F80_1C00]
  }

  pub fn set_register<T: Into<u32>>(&mut self, reg: T, val: u16) {
    self.registers[reg.into() as usize - 0x1F80_1C00] = val;
  }

  pub fn set_status_register(&mut self) {
    let mut status = 0;

    status |= self.get_register(SpuRegister::Control) & 0x3F;
    status |= ((self.capture_address as u16) & 0x100) << 3;

    self.set_register(SpuRegister::Status, status);
  }

  pub fn update_registers(&mut self) {
    self.set_status_register();
    self.set_register(SpuRegister::EndxLo, (self.endx >> 0) as u16);
    self.set_register(SpuRegister::EndxHi, (self.endx >> 16) as u16);
  }

  pub fn tick(&mut self, amount: i32, xa: &mut XaAdpcmDecoder) {
    self.prescaler += amount;

    while self.prescaler >= SPU_DIVIDER {
      self.prescaler -= SPU_DIVIDER;
      self.step(xa);
    }
  }

  pub fn step(&mut self, xa: &mut XaAdpcmDecoder) {
    let mut lsample = 0;
    let mut rsample = 0;

    for v in 0..24 {
      let mut l = 0;
      let mut r = 0;

      self.voice_tick(v, &mut l, &mut r);

      lsample += l;
      rsample += r;
    }

    {
      let (left, right) = xa.read();

      lsample += ((left as i32) * (self.cd_volume_left as i32)) / 32767;
      rsample += ((right as i32) * (self.cd_volume_right as i32)) / 32767;
    }

    self.key_on = 0;
    self.key_off = 0;

    self.samples.push(lsample.clamp(-32768, 32767) as i16);
    self.samples.push(rsample.clamp(-32768, 32767) as i16);

    self.capture_address = (self.capture_address + 1) & 0x1FF;

    // Update registers
    self.update_registers();
  }

  pub fn voice_tick(&mut self, v: usize, l: &mut i32, r: &mut i32) {
    self.voice_decoder_tick(v);

    let voice = &mut self.voices[v];
    let sample = voice.apply_envelope_raw();

    if v == 1 {
      self.ram.write(0x400 | self.capture_address, sample as u16);
    }

    if v == 3 {
      self.ram.write(0x600 | self.capture_address, sample as u16);
    }

    let (left, right) = voice.get_output();
    *l = ((sample as i32) * (left as i32)) >> 15;
    *r = ((sample as i32) * (right as i32)) >> 15;

    if voice.start_delay > 0 {
      voice.start_delay -= 1;
    } else {
      voice.tick();
    }

    let mask = 1 << v;

    if (self.key_off & mask) != 0 {
      self.key_off &= !mask;
      voice.key_off();
    }

    if (self.key_on & mask) != 0 {
      self.key_on &= !mask;
      self.endx &= !mask;
      voice.key_on();
    }
  }

  pub fn voice_decoder_tick(&mut self, v: usize) {
    let voice = &mut self.voices[v];
    if voice.fifo_size() >= 11 {
      return;
    }

    if (voice.current_address & 7) == 0 {
      if voice.header.loop_end {
        voice.current_address = voice.loop_address & !7;
        self.endx |= 1 << v;

        if !voice.header.loop_repeat {
          voice.restart();
        }
      }

      voice.set_header(self.ram.read(voice.current_address));
      voice.current_address += 1;
    }

    voice.set_sample(self.ram.read(voice.current_address));
    voice.current_address += 1;
  }
}

impl Comms for SpuCore {
  fn dma_speed(&mut self) -> i32 {
    4
  }

  fn dma_read_ready(&mut self) -> bool {
    true
  }

  fn dma_write_ready(&mut self) -> bool {
    true
  }

  fn dma_read(&mut self) -> u32 {
    0
  }

  fn dma_write(&mut self, val: u32) {
    // TODO: there should be a FIFO that gets filled/emptied to transfer words.

    let lo = (val >> 0) & 0xFFFF;
    let hi = (val >> 16) & 0xFFFF;

    self.io_write(AddressWidth::Half, SpuRegister::RamData.into(), lo);
    self.io_write(AddressWidth::Half, SpuRegister::RamData.into(), hi);
  }
}

impl IO for SpuCore {
  fn io_read(&mut self, width: AddressWidth, address: u32) -> u32 {
    timing_add_cpu_time(if width == AddressWidth::Word { 39 } else { 19 });

    if width == AddressWidth::Half {
      if address >= 0x1F80_1C00 && address <= 0x1F80_1D7F {
        let data = self.get_register(address);
        let v = &mut self.voices[((address as usize) >> 4) & 31];
        return v.io_read(address, data as u32);
      }

      return self.get_register(address) as u32;
    }

    panic!("");
  }

  fn io_write(&mut self, width: AddressWidth, address: u32, data: u32) {
    timing_add_cpu_time(if width == AddressWidth::Word { 39 } else { 19 });

    if width == AddressWidth::Word {
      self.io_write(AddressWidth::Half, address & !2, data);
      self.io_write(AddressWidth::Half, address | 2, data >> 16);
      return;
    }

    if width == AddressWidth::Half {
      self.set_register(address, data as u16);

      if address >= 0x1F80_1C00 && address <= 0x1F80_1D7F {
        let v = &mut self.voices[((address as usize) >> 4) & 31];
        v.io_write(address, data as u16);
        return;
      }

      match address.into() {
        SpuRegister::KonLo => {
          self.key_on &= 0xFF_0000;
          self.key_on |= data;
          return;
        }
        SpuRegister::KonHi => {
          self.key_on &= 0x00_FFFF;
          self.key_on |= data << 16;
          return;
        }
        SpuRegister::KoffLo => {
          self.key_off &= 0xFF_0000;
          self.key_off |= data;
          return;
        }
        SpuRegister::KoffHi => {
          self.key_off &= 0x00_FFFF;
          self.key_off |= data << 16;
          return;
        }
        SpuRegister::PmonLo => {
          self.pmon &= 0xFF_0000;
          self.pmon |= data;
          return;
        }
        SpuRegister::PmonHi => {
          self.pmon &= 0x00_FFFF;
          self.pmon |= data << 16;
          return;
        }
        SpuRegister::RamAddrIrq => {
          self.ram_address_irq = SoundRamAddress::create(data as u16);
          return;
        }
        SpuRegister::RamAddr => {
          self.ram_address = SoundRamAddress::create(data as u16);
          return;
        }
        SpuRegister::RamData => {
          self.ram.write(self.ram_address, data as u16);
          self.ram_address += 1;
          return;
        }
        SpuRegister::CdVolumeLeft => {
          self.cd_volume_left = data as u16 as i16;
          return;
        }
        SpuRegister::CdVolumeRight => {
          self.cd_volume_right = data as u16 as i16;
          return;
        }
        _ => {
          return;
        }
      }
    }

    panic!("");
  }
}
