pub struct AdpcmHeader {
  pub shift: i32,
  pub filter: i32,
  pub loop_end: bool,
  pub loop_repeat: bool,
  pub loop_start: bool,
}

impl AdpcmHeader {
  pub fn create(header: u16) -> AdpcmHeader {
    AdpcmHeader {
      shift: (header & 15) as i32,
      filter: ((header >> 4) & 15) as i32,
      loop_end: (header & (1 << 8)) != 0,
      loop_repeat: (header & (1 << 9)) != 0,
      loop_start: (header & (1 << 10)) != 0,
    }
  }
}

pub struct AdpcmSample {
  pub coded: [i16; 4],
}

impl AdpcmSample {
  pub fn create(sample: u16) -> AdpcmSample {
    AdpcmSample {
      coded: [
        ((sample << 12) & 0xF000) as i16,
        ((sample << 8) & 0xF000) as i16,
        ((sample << 4) & 0xF000) as i16,
        ((sample << 0) & 0xF000) as i16,
      ],
    }
  }
}
