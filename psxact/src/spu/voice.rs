use crate::{
  spu::{
    adpcm::{AdpcmHeader, AdpcmSample},
    adsr_envelope::AdsrEnvelope,
    gauss,
    sound_ram::SoundRamAddress,
    volume::Volume,
  },
  util::fifo::Fifo,
};

pub struct Voice {
  pub adsr: AdsrEnvelope,
  pub volume_left: Volume,
  pub volume_right: Volume,

  pub pitch: u16,
  pub phase: u32,

  pub decoder_fifo: Fifo<i16, 4>,
  pub current_address: u32,
  pub loop_address: u32,
  pub start_address: u32,
  pub last_samples: [i16; 2],

  pub start_delay: u32,

  pub header: AdpcmHeader,
}

impl Voice {
  pub fn new() -> Voice {
    Voice {
      adsr: AdsrEnvelope::new(),
      volume_left: Volume::new(),
      volume_right: Volume::new(),
      pitch: 0,
      phase: 0,
      decoder_fifo: Fifo::new(),
      current_address: 0,
      loop_address: 0,
      start_address: 0,
      last_samples: [0; 2],
      start_delay: 0,
      header: AdpcmHeader::create(0),
    }
  }

  pub fn set_header(&mut self, val: u16) {
    self.header = AdpcmHeader::create(val);

    if self.header.loop_start {
      self.loop_address = self.current_address & !7;
    }
  }

  pub fn set_sample(&mut self, mut val: u16) {
    // decode
    let wp = POS_XA_ADPCM_TABLE[self.header.filter as usize];
    let wn = NEG_XA_ADPCM_TABLE[self.header.filter as usize];
    let mut shift = self.header.shift;

    if shift > 12 {
      val &= 0x8888;
      shift = 8;
    }

    let sample = AdpcmSample::create(val);

    for &coded in sample.coded.iter() {
      let mut out = (coded as i32) >> shift;
      out += ((self.last_samples[0] as i32) * wp) >> 6;
      out += ((self.last_samples[1] as i32) * wn) >> 6;
      out = {
        if out < -0x8000 {
          -0x8000
        } else if out > 0x7FFF {
          0x7FFF
        } else {
          out
        }
      };

      self.decoder_fifo.write(out as i16);

      self.last_samples[1] = self.last_samples[0];
      self.last_samples[0] = out as i16;
    }
  }

  pub fn raw_sample(&self) -> i32 {
    let index = (self.phase >> 4) & 0xFF;

    return gauss::filter(
      index as i32,
      self.decoder_fifo.at(0),
      self.decoder_fifo.at(1),
      self.decoder_fifo.at(2),
      self.decoder_fifo.at(3),
    );
  }

  pub fn apply_envelope(&self, raw: i32) -> i32 {
    let level = self.adsr.get_level() as i32;
    (level * raw) >> 15
  }

  pub fn counter_step(&mut self) {
    let mut r = self.pitch as u32;

    // TODO: pitch modulation

    if r > 0x3FFF {
      r = 0x3FFF;
    }

    let p = self.phase + r;
    let s = p >> 12;

    self.phase = p & 0xFFF;
    self.decoder_fifo.discard(s as usize);
  }

  pub fn apply_envelope_raw(&mut self) -> i32 {
    let raw = self.raw_sample();
    self.apply_envelope(raw)
  }

  pub fn tick(&mut self) {
    self.adsr.step();
    self.counter_step();
  }

  pub fn get_output(&mut self) -> (i16, i16) {
    let left = self.volume_left.get_level();
    let right = self.volume_right.get_level();
    (left, right)
  }

  pub fn key_on(&mut self) {
    self.adsr.key_on();
    self.start_delay = 4;
    self.phase = 0;
    self.header = AdpcmHeader::create(0);
    self.decoder_fifo.clear();
    self.last_samples[0] = 0;
    self.last_samples[1] = 0;
    self.current_address = self.start_address & !7;
  }

  pub fn key_off(&mut self) {
    self.adsr.key_off();
  }

  pub fn restart(&mut self) {
    self.adsr.key_off();
    self.adsr.set_level(0);
  }

  pub fn fifo_size(&mut self) -> usize {
    self.decoder_fifo.size()
  }

  pub fn io_read(&mut self, address: u32, default_data: u32) -> u32 {
    match (address >> 1) & 7 {
      0 => default_data,
      1 => default_data,
      2 => default_data,
      3 => default_data,
      4 => default_data,
      5 => default_data,
      6 => self.adsr.get_level() as u32,
      _ => self.loop_address,
    }
  }

  pub fn io_write(&mut self, address: u32, data: u16) {
    match (address >> 1) & 7 {
      0 => self.volume_left.set_level(data),
      1 => self.volume_right.set_level(data),
      2 => self.pitch = data,
      3 => self.start_address = SoundRamAddress::create(data & !1),
      4 => self.adsr.set_config_lo(data),
      5 => self.adsr.set_config_hi(data),
      6 => self.adsr.set_level(data as i16),
      _ => self.loop_address = SoundRamAddress::create(data & !1),
    }
  }
}

const POS_XA_ADPCM_TABLE: [i32; 16] = [0, 60, 115, 98, 122, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

const NEG_XA_ADPCM_TABLE: [i32; 16] = [0, 0, -52, -55, -60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
