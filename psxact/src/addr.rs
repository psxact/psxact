pub enum Addr {
  Bios(u32),
  Cdrom(CdromAddr),
  Dma(DmaAddr),
  Exp1(u32),
  Exp2(u32),
  Exp3(u32),
  Gpu(GpuAddr),
  Mdec(MdecAddr),
  Timer(TimerAddr),
  Wram(u32),
  Invalid(u32),
}

impl Addr {
  pub fn decode(address: u32) -> Addr {
    match address {
      0x0000_0000..=0x001F_FFFF => Addr::Wram(address),
      0x1F00_0000..=0x1F7F_FFFF => Addr::Exp1(address),
      0x1F80_1080..=0x1F80_10FF => {
        let idx = (address >> 4) & 7;
        let reg = (address >> 2) & 3;

        if idx == 7 {
          match reg {
            0 => Addr::Dma(DmaAddr::PriorityControl),
            1 => Addr::Dma(DmaAddr::InterruptControl),
            2 => Addr::Dma(DmaAddr::Garbage1),
            _ => Addr::Dma(DmaAddr::Garbage2),
          }
        } else {
          match reg {
            0 => Addr::Dma(DmaAddr::ChannelAddress(idx)),
            1 => Addr::Dma(DmaAddr::ChannelLength(idx)),
            _ => Addr::Dma(DmaAddr::ChannelControl(idx)),
          }
        }
      }
      0x1F80_1100..=0x1F80_112F => {
        let idx = (address >> 4) & 3;
        let reg = (address >> 2) & 3;

        match reg {
          0 => Addr::Timer(TimerAddr::Counter(idx)),
          1 => Addr::Timer(TimerAddr::Control(idx)),
          2 => Addr::Timer(TimerAddr::Maximum(idx)),
          _ => Addr::Invalid(address),
        }
      }
      0x1F80_1800 => Addr::Cdrom(CdromAddr::Port0),
      0x1F80_1801 => Addr::Cdrom(CdromAddr::Port1),
      0x1F80_1802 => Addr::Cdrom(CdromAddr::Port2),
      0x1F80_1803 => Addr::Cdrom(CdromAddr::Port3),
      0x1F80_1810..=0x1F80_1813 => Addr::Gpu(GpuAddr::ReadGp0),
      0x1F80_1814..=0x1F80_1817 => Addr::Gpu(GpuAddr::StatGp1),
      0x1F80_1820..=0x1F80_1823 => Addr::Mdec(MdecAddr::Mdec0),
      0x1F80_1824..=0x1F80_1827 => Addr::Mdec(MdecAddr::Mdec1),
      0x1F80_2000..=0x1F80_3FFF => Addr::Exp2(address),
      0x1FA0_0000..=0x1FBF_FFFF => Addr::Exp3(address),
      0x1FC0_0000..=0x1FFF_FFFF => Addr::Bios(address),
      _ => Addr::Invalid(address),
    }
  }
}

pub enum CdromAddr {
  Port0,
  Port1,
  Port2,
  Port3,
}

pub enum DmaAddr {
  ChannelAddress(u32),
  ChannelLength(u32),
  ChannelControl(u32),
  PriorityControl,
  InterruptControl,
  Garbage1,
  Garbage2,
}

pub enum GpuAddr {
  ReadGp0,
  StatGp1,
}

pub enum MdecAddr {
  Mdec0,
  Mdec1,
}

pub enum TimerAddr {
  Counter(u32),
  Control(u32),
  Maximum(u32),
}
