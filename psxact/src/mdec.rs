use crate::{timing::timing_add_cpu_time, util::fifo::Fifo, AddressWidth, Comms, IO};

pub struct MdecCommand {
  val: u32,
  out: u32,
  len: u16,
  run: bool,
}

impl MdecCommand {
  pub fn new() -> MdecCommand {
    MdecCommand {
      val: 0,
      out: 0,
      len: 0,
      run: false,
    }
  }
}

pub struct MdecCore {
  status: u32,
  dma_0_enabled: bool,
  dma_1_enabled: bool,

  cmd: MdecCommand,

  data_in: Fifo<u32, 5>,
  data_out: Fifo<u32, 5>,

  color_table: [u8; 64],
  light_table: [u8; 64],
  scale_table: [i16; 64],
}

impl MdecCore {
  pub fn new() -> MdecCore {
    MdecCore {
      status: 0,
      dma_0_enabled: false,
      dma_1_enabled: false,
      cmd: MdecCommand::new(),
      data_in: Fifo::new(),
      data_out: Fifo::new(),
      color_table: [0; 64],
      light_table: [0; 64],
      scale_table: [0; 64],
    }
  }

  pub fn get_status(&self) -> u32 {
    let bit_31 = self.data_out.is_empty() as u32;
    let bit_30 = self.data_in.is_full() as u32;
    let bit_29 = self.cmd.run as u32;
    let bit_28 = (self.dma_0_enabled && self.cmd.run && !self.data_in.is_full()) as u32;
    let bit_27 = (self.dma_1_enabled && self.cmd.run && !self.data_out.is_empty()) as u32; // TODO: probably not just while a command is active?
    let bit_23 = self.cmd.out;
    let bit_00 = self.cmd.len as u32;

    return self.status
      | (bit_31 << 31)
      | (bit_30 << 30)
      | (bit_29 << 29)
      | (bit_28 << 28)
      | (bit_27 << 27)
      | (bit_23 << 23)
      | (bit_00 << 0);
  }

  pub fn set_command(&mut self, val: u32) {
    self.cmd.val = (val >> 29) & 0x7;
    self.cmd.out = (val >> 25) & 0xF;
    self.cmd.len = val as u16;

    if self.cmd.val == 2 {
      self.cmd.len = if (val & 1) != 0 { 32 } else { 16 };
    }

    if self.cmd.val == 3 {
      self.cmd.len = 32;
    }

    if self.cmd.val == 1 || self.cmd.val == 2 || self.cmd.val == 3 {
      self.cmd.run = true;
      self.cmd.len -= 1;
    }
  }

  pub fn set_parameter(&mut self, val: u32) {
    if self.data_in.is_full() {
      // For at least command 1, the MDEC appears to wait for a full input FIFO
      // before processing anything.
      self.run_command();
    }

    // write parameter
    self.data_in.write(val);

    if self.cmd.len == 0 {
      // run a command here?
      self.run_command();
      self.cmd.run = false;
    }

    self.cmd.len -= 1;
  }

  pub fn set_control(&mut self, val: u32) {
    self.dma_0_enabled = ((val >> 30) & 1) != 0;
    self.dma_1_enabled = ((val >> 29) & 1) != 0;

    if (val & 0x8000_0000) != 0 {
      self.status = 0x4_0000;
      self.data_in.clear();
      self.data_out.clear();
      self.cmd = MdecCommand::new();
      self.dma_0_enabled = false;
      self.dma_1_enabled = false;
    }
  }

  pub fn run_command(&mut self) {
    if self.cmd.val == 1 {
      assert!(self.data_in.size() == 32, "mdec command with wrong parameter count");

      self.data_in.clear();
    }

    if self.cmd.val == 2 {
      let len = self.data_in.size();
      if len != 16 && len != 32 {
        panic!("Unhandled MDEC FIFO size for command 2.");
      } else if len == 16 {
        self.fill_light_table();
      } else if len == 32 {
        self.fill_light_table();
        self.fill_color_table();
      }
    }

    if self.cmd.val == 3 {
      self.fill_scale_table();
    }

    assert!(self.data_in.is_empty(), "junk data left in mdec parameter fifo");
  }

  pub fn fill_light_table(&mut self) {
    // We can't just assert the actual data size here because color data might
    // follow.

    assert!(
      self.data_in.size() == 16 || self.data_in.size() == 32,
      "fill light table with wrong parameter count"
    );

    for i in (0..64).step_by(4) {
      let val = self.data_in.read();
      self.light_table[i + 0] = (val >> (8 * 0)) as u8;
      self.light_table[i + 1] = (val >> (8 * 1)) as u8;
      self.light_table[i + 2] = (val >> (8 * 2)) as u8;
      self.light_table[i + 3] = (val >> (8 * 3)) as u8;
    }
  }

  pub fn fill_color_table(&mut self) {
    assert!(self.data_in.size() == 16, "fill color table with wrong parameter count");

    for i in (0..64).step_by(4) {
      let val = self.data_in.read();
      self.color_table[i + 0] = (val >> (8 * 0)) as u8;
      self.color_table[i + 1] = (val >> (8 * 1)) as u8;
      self.color_table[i + 2] = (val >> (8 * 2)) as u8;
      self.color_table[i + 3] = (val >> (8 * 3)) as u8;
    }
  }

  pub fn fill_scale_table(&mut self) {
    assert!(self.data_in.size() == 32, "fill scale table with wrong parameter count");

    for i in (0..64).step_by(2) {
      let val = self.data_in.read();
      self.scale_table[i + 0] = (val >> 0) as i16;
      self.scale_table[i + 1] = (val >> 16) as i16;
    }
  }
}

impl Comms for MdecCore {
  fn dma_speed(&mut self) -> i32 {
    1
  }

  fn dma_read_ready(&mut self) -> bool {
    true
  }

  fn dma_write_ready(&mut self) -> bool {
    true
  }

  fn dma_read(&mut self) -> u32 {
    // panic("MDEC DMA read isn't implemented.");
    0
  }

  fn dma_write(&mut self, val: u32) {
    self.io_write(AddressWidth::Word, 0x1F80_1820, val);
  }
}

impl IO for MdecCore {
  fn io_read(&mut self, width: AddressWidth, address: u32) -> u32 {
    timing_add_cpu_time(4);

    if let AddressWidth::Word = width {
      if address == 0x1F80_1824 {
        return self.get_status();
      }
    }

    panic!("")
  }

  fn io_write(&mut self, width: AddressWidth, address: u32, data: u32) {
    timing_add_cpu_time(4);

    if let AddressWidth::Word = width {
      match address {
        0x1F80_1820 => {
          if self.cmd.run {
            return self.set_parameter(data);
          } else {
            return self.set_command(data);
          }
        }
        0x1F80_1824 => {
          return self.set_control(data);
        }
        _ => {}
      }
    }

    panic!("")
  }
}
