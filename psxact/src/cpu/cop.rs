pub trait CoProcessor {
  fn cop_is_present(&self) -> bool;

  fn cop_run(&mut self, code: u32);

  fn cop_read_gpr(&mut self, n: u32) -> u32;

  fn cop_write_gpr(&mut self, n: u32, val: u32);

  fn cop_read_ccr(&mut self, n: u32) -> u32;

  fn cop_write_ccr(&mut self, n: u32, val: u32);
}

pub struct VoidCoProcessor;

impl CoProcessor for VoidCoProcessor {
  fn cop_is_present(&self) -> bool {
    false
  }

  fn cop_run(&mut self, _: u32) {
    panic!("void co-processor invoked")
  }

  fn cop_read_gpr(&mut self, _: u32) -> u32 {
    panic!("void co-processor invoked")
  }

  fn cop_write_gpr(&mut self, _: u32, _: u32) {
    panic!("void co-processor invoked")
  }

  fn cop_read_ccr(&mut self, _: u32) -> u32 {
    panic!("void co-processor invoked")
  }

  fn cop_write_ccr(&mut self, _: u32, _: u32) {
    panic!("void co-processor invoked")
  }
}
