use super::cop::CoProcessor;

pub enum Exception {
  Interrupt,
  TlbModification,
  TlbLoad,
  TlbStore,
  AddressErrorLoad,
  AddressErrorStore,
  InstructionBusError,
  DataBusError,
  SysCall,
  Breakpoint,
  ReservedInstruction,
  CopUnusable,
  Overflow,
}

pub struct SysCore {
  bpc: u32,
  bda: u32,
  tar: u32,
  dcic: u32,
  bad_vaddr: u32,
  bdam: u32,
  bpcm: u32,
  sr: u32,
  cause: u32,
  epc: u32,
}

impl SysCore {
  pub fn new() -> SysCore {
    SysCore {
      bpc: 0,
      bda: 0,
      tar: 0,
      dcic: 0,
      bad_vaddr: 0,
      bdam: 0,
      bpcm: 0,
      sr: 0,
      cause: 0,
      epc: 0,
    }
  }

  pub fn run(&mut self, n: u32) {
    if n == 0x10 {
      return self.pop_flags();
    }
  }

  pub fn read_ccr(&mut self, _: u32) -> u32 {
    panic!("Tried to read unimplemented COP0 register, should trigger RI exception.");
  }

  pub fn write_ccr(&mut self, _: u32, _: u32) {
    panic!("Tried to write unimplemented COP0 register, should trigger RI exception.");
  }

  pub fn read_gpr(&mut self, n: u32) -> u32 {
    match n {
      0x3 => self.bpc,       // BPC (Read-write)
      0x5 => self.bda,       // BDA (Read-write)
      0x6 => self.tar,       // TAR (Read-only)
      0x7 => self.dcic,      // DCIC (Read-write)
      0x8 => self.bad_vaddr, // BadVaddr (Read-only)
      0x9 => self.bdam,      // BDAM (Read-write)
      0xB => self.bpcm,      // BPCM (Read-write)
      0xC => self.sr,        // SR (Read-write)
      0xD => self.cause,     // CAUSE (Read-only)
      0xE => self.epc,       // EPC (Read-only)
      0xF => 0x0000_0002,    // PRID (Read-only)
      _ => {
        panic!("Tried to read unimplemented COP0 register, should trigger RI exception.")
      }
    }
  }

  pub fn write_gpr(&mut self, n: u32, val: u32) {
    match n {
      0x3 => self.bpc = val,
      0x5 => self.bda = val,
      0x6..=0x7 => self.dcic = val,
      0x8..=0x9 => self.bdam = val,
      0xB => self.bpcm = val,
      0xC => self.sr = val,
      0xD =>
      // -0-- 0000 0000 0000 ---- --xx 0--- --00
      {
        self.cause = (self.cause & 0xB000_FC7C) | (val & 0x0000_0300)
      }

      0xE => (),
      0xF => (),
      _ => {
        panic!("Tried to write unimplemented COP0 register, should trigger RI exception.")
      }
    }
  }

  pub fn get_iec(&self) -> bool {
    (self.sr & (1 << 0)) != 0
  }

  pub fn get_irq(&self) -> bool {
    (self.sr & self.cause & 0xFF00) != 0
  }

  pub fn get_isc(&self) -> bool {
    (self.sr & (1 << 16)) != 0
  }

  pub fn get_swc(&self) -> bool {
    (self.sr & (1 << 17)) != 0
  }

  pub fn get_bev(&self) -> bool {
    (self.sr & (1 << 22)) != 0
  }

  pub fn set_tar(&mut self, val: u32) {
    self.tar = val;
  }

  pub fn set_cause_excode(&mut self, val: Exception) {
    self.cause = (self.cause & !0x7C) | ((val as u32) << 2);
  }

  pub fn set_cause_ip(&mut self, val: bool) {
    self.cause = (self.cause & !0x400) | ((val as u32) << 10);
  }

  pub fn set_cause_ce(&mut self, val: u32) {
    self.cause = (self.cause & !0x3000_0000) | ((val & 3) << 28);
  }

  pub fn set_cause_bt(&mut self, val: bool) {
    self.cause = (self.cause & !0x4000_0000) | ((val as u32) << 30);
  }

  pub fn set_cause_bd(&mut self, val: bool) {
    self.cause = (self.cause & !0x8000_0000) | ((val as u32) << 31);
  }

  pub fn set_epc(&mut self, val: u32) {
    self.epc = val;
  }

  pub fn push_flags(&mut self) {
    self.sr = (self.sr & !0x3F) | ((self.sr << 2) & 0x3F);
  }

  pub fn pop_flags(&mut self) {
    self.sr = (self.sr & !0xF) | ((self.sr >> 2) & 0xF);
  }
}

impl CoProcessor for SysCore {
  fn cop_is_present(&self) -> bool {
    true
  }

  fn cop_run(&mut self, code: u32) {
    self.run(code)
  }

  fn cop_read_gpr(&mut self, n: u32) -> u32 {
    self.read_gpr(n)
  }

  fn cop_write_gpr(&mut self, n: u32, val: u32) {
    self.write_gpr(n, val)
  }

  fn cop_read_ccr(&mut self, n: u32) -> u32 {
    self.read_ccr(n)
  }

  fn cop_write_ccr(&mut self, n: u32, val: u32) {
    self.write_ccr(n, val)
  }
}
