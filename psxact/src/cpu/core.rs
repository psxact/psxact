use crate::{
  cpu::{
    cop::{CoProcessor, VoidCoProcessor},
    decoder::*,
    gte::GteCore,
    register_file::*,
    segment::Segment,
    sys::{Exception, SysCore},
  },
  memory::MemoryBase,
  timing::{timing_add_cpu_time, timing_get_cpu_time, timing_reset_cpu_time},
  AddressWidth, InterruptType, IO,
};

pub const DCACHE_SIZE: usize = 1024;

pub enum IoTarget {
  Memory,
  ICache,
  DCache,
  CpuReg,
}

pub struct CpuCoreRegs {
  lo: u32,
  hi: u32,
  pc: u32,
  this_pc: u32,
  next_pc: u32,
}

impl CpuCoreRegs {
  fn new() -> CpuCoreRegs {
    CpuCoreRegs {
      lo: 0,
      hi: 0,
      pc: 0xBFC0_0000,
      this_pc: 0,
      next_pc: 0xBFC0_0004,
    }
  }
}

pub struct CpuCore {
  dcache: MemoryBase,
  edges: u32,

  cop0: SysCore,
  cop1: VoidCoProcessor,
  cop2: GteCore,
  cop3: VoidCoProcessor,

  regs: CpuCoreRegs,

  rf: CpuRegisterFile,

  code: CpuInstr,

  is_branch: bool,
  is_branch_delay_slot: bool,
  is_branch_taken: bool,
  branch_target: u32,

  is_load: bool,
  is_load_delay_slot: bool,
  load_index: u32,
  load_value: u32,

  istat: u32,
  imask: u32,
}

impl CpuCore {
  pub fn new() -> CpuCore {
    CpuCore {
      dcache: MemoryBase::ram(DCACHE_SIZE),
      edges: 0,
      cop0: SysCore::new(),
      cop1: VoidCoProcessor,
      cop2: GteCore::new(),
      cop3: VoidCoProcessor,
      regs: CpuCoreRegs::new(),
      rf: CpuRegisterFile::new(),
      code: CpuInstr::new(0),
      is_branch: false,
      is_branch_delay_slot: false,
      is_branch_taken: false,
      branch_target: 0,
      is_load: false,
      is_load_delay_slot: false,
      load_index: 0,
      load_value: 0,
      istat: 0,
      imask: 0,
    }
  }

  pub fn tick(&mut self, memory: &mut dyn IO) -> i32 {
    timing_reset_cpu_time();

    self.read_code(memory);

    self.is_branch_delay_slot = self.is_branch;
    self.is_branch = false;

    self.is_load_delay_slot = self.is_load;
    self.is_load = false;

    if self.cop0.get_iec() && self.cop0.get_irq() && !self.code.is_gte() {
      self.enter_exception(Exception::Interrupt, 0);
    } else {
      let opc = self.code.opc();
      if opc != 0 {
        match opc {
          0x00 => (), /* escape sequence for table below. */
          0x01 => self.op_bxx(),
          0x02 => self.op_j(),
          0x03 => self.op_jal(),
          0x04 => self.op_beq(),
          0x05 => self.op_bne(),
          0x06 => self.op_blez(),
          0x07 => self.op_bgtz(),
          0x08 => self.op_addi(),
          0x09 => self.op_addiu(),
          0x0A => self.op_slti(),
          0x0B => self.op_sltiu(),
          0x0C => self.op_andi(),
          0x0D => self.op_ori(),
          0x0E => self.op_xori(),
          0x0F => self.op_lui(),
          0x10 => self.op_cop0(),
          0x11 => self.op_cop1(),
          0x12 => self.op_cop2(),
          0x13 => self.op_cop3(),
          0x14 => self.op_und(),
          0x15 => self.op_und(),
          0x16 => self.op_und(),
          0x17 => self.op_und(),
          0x18 => self.op_und(),
          0x19 => self.op_und(),
          0x1A => self.op_und(),
          0x1B => self.op_und(),
          0x1C => self.op_und(),
          0x1D => self.op_und(),
          0x1E => self.op_und(),
          0x1F => self.op_und(),
          0x20 => self.op_lb(memory),
          0x21 => self.op_lh(memory),
          0x22 => self.op_lwl(memory),
          0x23 => self.op_lw(memory),
          0x24 => self.op_lbu(memory),
          0x25 => self.op_lhu(memory),
          0x26 => self.op_lwr(memory),
          0x27 => self.op_und(),
          0x28 => self.op_sb(memory),
          0x29 => self.op_sh(memory),
          0x2A => self.op_swl(memory),
          0x2B => self.op_sw(memory),
          0x2C => self.op_und(),
          0x2D => self.op_und(),
          0x2E => self.op_swr(memory),
          0x2F => self.op_und(),
          0x30 => self.op_lwc0(memory),
          0x31 => self.op_lwc1(memory),
          0x32 => self.op_lwc2(memory),
          0x33 => self.op_lwc3(memory),
          0x34 => self.op_und(),
          0x35 => self.op_und(),
          0x36 => self.op_und(),
          0x37 => self.op_und(),
          0x38 => self.op_swc0(memory),
          0x39 => self.op_swc1(memory),
          0x3A => self.op_swc2(memory),
          0x3B => self.op_swc3(memory),
          0x3C => self.op_und(),
          0x3D => self.op_und(),
          0x3E => self.op_und(),
          _ => self.op_und(),
        }
      } else {
        match self.code.opc_special() {
          0x00 => self.op_sll(),
          0x01 => self.op_und(),
          0x02 => self.op_srl(),
          0x03 => self.op_sra(),
          0x04 => self.op_sllv(),
          0x05 => self.op_und(),
          0x06 => self.op_srlv(),
          0x07 => self.op_srav(),
          0x08 => self.op_jr(),
          0x09 => self.op_jalr(),
          0x0A => self.op_und(),
          0x0B => self.op_und(),
          0x0C => self.op_syscall(),
          0x0D => self.op_break(),
          0x0E => self.op_und(),
          0x0F => self.op_und(),
          0x10 => self.op_mfhi(),
          0x11 => self.op_mthi(),
          0x12 => self.op_mflo(),
          0x13 => self.op_mtlo(),
          0x14 => self.op_und(),
          0x15 => self.op_und(),
          0x16 => self.op_und(),
          0x17 => self.op_und(),
          0x18 => self.op_mult(),
          0x19 => self.op_multu(),
          0x1A => self.op_div(),
          0x1B => self.op_divu(),
          0x1C => self.op_und(),
          0x1D => self.op_und(),
          0x1E => self.op_und(),
          0x1F => self.op_und(),
          0x20 => self.op_add(),
          0x21 => self.op_addu(),
          0x22 => self.op_sub(),
          0x23 => self.op_subu(),
          0x24 => self.op_and(),
          0x25 => self.op_or(),
          0x26 => self.op_xor(),
          0x27 => self.op_nor(),
          0x28 => self.op_und(),
          0x29 => self.op_und(),
          0x2A => self.op_slt(),
          0x2B => self.op_sltu(),
          0x2C => self.op_und(),
          0x2D => self.op_und(),
          0x2E => self.op_und(),
          0x2F => self.op_und(),
          0x30 => self.op_und(),
          0x31 => self.op_und(),
          0x32 => self.op_und(),
          0x33 => self.op_und(),
          0x34 => self.op_und(),
          0x35 => self.op_und(),
          0x36 => self.op_und(),
          0x37 => self.op_und(),
          0x38 => self.op_und(),
          0x39 => self.op_und(),
          0x3A => self.op_und(),
          0x3B => self.op_und(),
          0x3C => self.op_und(),
          0x3D => self.op_und(),
          0x3E => self.op_und(),
          _ => self.op_und(),
        }
      }
    }

    return timing_get_cpu_time();
  }

  pub fn interrupt(&mut self, int: InterruptType) {
    let istat = self.get_istat() | (int as u32);
    self.set_istat(istat);
  }

  pub fn ack(&mut self, int: InterruptType) {
    let bit: u32 = int as u32;
    self.edges &= !bit;
  }

  pub fn irq(&mut self, int: InterruptType) {
    let bit: u32 = int as u32;

    if (self.edges & bit) == 0 {
      self.interrupt(int);
    }

    self.edges |= bit;
  }

  pub fn get_pc(&self) -> u32 {
    return self.regs.pc;
  }

  pub fn get_register(&self, index: u32) -> u32 {
    if self.is_load_delay_slot && self.load_index == index {
      return self.load_value;
    } else {
      return self.rf.get(index);
    }
  }

  pub fn set_pc(&mut self, value: u32) {
    self.regs.this_pc = value;
    self.regs.pc = value;
    self.regs.next_pc = value + 4;
  }

  pub fn set_register(&mut self, index: u32, value: u32) {
    self.rf.set(index, value);
  }

  pub fn io_read(&mut self, width: AddressWidth, address: u32) -> u32 {
    timing_add_cpu_time(4);

    if width == AddressWidth::Word || width == AddressWidth::Half {
      match address {
        0x1F80_1070 => return self.get_istat(),
        0x1F80_1074 => return self.get_imask(),
        _ => panic!(),
      }
    }

    panic!();
  }

  pub fn io_write(&mut self, width: AddressWidth, address: u32, val: u32) {
    timing_add_cpu_time(4);

    if width == AddressWidth::Word || width == AddressWidth::Half {
      match address {
        0x1F80_1070 => return self.set_istat(val & self.istat),
        0x1F80_1074 => return self.set_imask(val & 0x7FF),
        _ => panic!(),
      }
    }

    panic!();
  }

  fn get_code(&mut self) -> CpuInstr {
    return self.code;
  }

  fn get_rt(&mut self) -> u32 {
    return self.get_register(self.code.rt());
  }

  fn get_rt_forwarded(&mut self) -> u32 {
    return self.get_register_forwarded(self.code.rt());
  }

  fn get_rs(&mut self) -> u32 {
    return self.get_register(self.code.rs());
  }

  fn get_register_forwarded(&mut self, index: u32) -> u32 {
    return self.rf.get(index);
  }

  fn set_rd(&mut self, value: u32) {
    self.rf.set(self.code.rd(), value);
  }

  fn set_rt(&mut self, value: u32) {
    self.rf.set(self.code.rt(), value);
  }

  fn set_rt_load(&mut self, value: u32) {
    let t = self.code.rt();

    if self.is_load_delay_slot && self.load_index == t {
      self.rf.set(t, self.load_value);
    }

    self.is_load = true;
    self.load_index = t;
    self.load_value = self.rf.get(t);

    self.rf.set(t, value);
  }

  fn update_irq(&mut self, stat: u32, mask: u32) {
    self.istat = stat;
    self.imask = mask;
    self.cop0.set_cause_ip((self.istat & self.imask) != 0);
  }

  fn get_imask(&mut self) -> u32 {
    return self.imask;
  }

  fn set_imask(&mut self, mask: u32) {
    let stat = self.get_istat();
    self.update_irq(stat, mask);
  }

  fn get_istat(&self) -> u32 {
    return self.istat;
  }

  fn set_istat(&mut self, stat: u32) {
    let mask = self.get_imask();
    self.update_irq(stat, mask);
  }

  fn get_cop(&mut self, n: u32) -> &mut dyn CoProcessor {
    match n {
      0 => &mut self.cop0,
      1 => &mut self.cop1,
      2 => &mut self.cop2,
      _ => &mut self.cop3,
    }
  }

  fn get_cop_usable(&mut self, n: u32) -> bool {
    if !self.get_cop(n).cop_is_present() {
      return false;
    }

    let stat = self.get_cop(0).cop_read_gpr(12);
    let mask = 1 << (28 + n);

    return if n == 0 && (stat & 0x1000_0000) == 0 {
      (stat & 0x02) == 0
    } else {
      (stat & mask) != 0
    };
  }

  fn enter_exception(&mut self, code: Exception, cop: u32) {
    self.cop0.push_flags();
    self.cop0.set_cause_excode(code);
    self.cop0.set_cause_ce(cop);
    self.cop0.set_cause_bt(self.is_branch_delay_slot && self.is_branch_taken);
    self.cop0.set_cause_bd(self.is_branch_delay_slot);

    if self.is_branch_delay_slot {
      if self.is_branch_taken {
        self.cop0.set_tar(self.branch_target);
      } else {
        self.cop0.set_tar(self.regs.this_pc + 4);
      }

      self.cop0.set_epc(self.regs.this_pc - 4);
    } else {
      self.cop0.set_epc(self.regs.this_pc);
    }

    if self.cop0.get_bev() {
      self.set_pc(0xBFC0_0180);
    } else {
      self.set_pc(0x8000_0080);
    }
  }

  fn read_code(&mut self, memory: &mut dyn IO) {
    if (self.regs.pc & 3) != 0 {
      return self.enter_exception(Exception::AddressErrorLoad, 0);
    }

    self.regs.this_pc = self.regs.pc;
    self.regs.pc = self.regs.next_pc;
    self.regs.next_pc += 4;

    // TODO: read i-cache

    self.code = CpuInstr::new(memory.io_read(AddressWidth::Word, map_address(self.regs.this_pc)));
  }

  fn get_target(&mut self, address: u32) -> IoTarget {
    if self.cop0.get_isc() {
      if self.cop0.get_swc() {
        return IoTarget::ICache;
      } else {
        return IoTarget::DCache;
      }
    }

    if (address & 0x7FFF_FC00) == 0x1F80_0000 && Segment::is_cached(address) {
      return IoTarget::DCache;
    }

    // Check for internal register accesses.

    if (address & 0x7FFF_FFF8) == 0x1F80_1070 {
      return IoTarget::CpuReg;
    }

    return IoTarget::Memory;
  }

  fn read_data(&mut self, memory: &mut dyn IO, width: AddressWidth, address: u32) -> u32 {
    let address = map_address(address);

    match self.get_target(address) {
      IoTarget::ICache => 0,
      IoTarget::DCache => self.dcache.io_read(width, address),
      IoTarget::Memory => memory.io_read(width, address),
      IoTarget::CpuReg => self.io_read(width, address),
    }
  }

  fn write_data(&mut self, memory: &mut dyn IO, width: AddressWidth, address: u32, val: u32) {
    let address = map_address(address);

    match self.get_target(address) {
      IoTarget::ICache => (),
      IoTarget::DCache => self.dcache.io_write(width, address, val),
      IoTarget::Memory => memory.io_write(width, address, val),
      IoTarget::CpuReg => self.io_write(width, address, val),
    }
  }

  // --========--
  //   Decoding
  // --========--

  fn branch(&mut self, target: u32, condition: bool) {
    self.is_branch = true;
    self.is_branch_taken = condition;
    self.branch_target = target;

    if condition {
      self.regs.next_pc = target;
    }
  }

  fn branch_abs(&mut self) -> u32 {
    return (self.regs.pc & 0xF000_0000) | self.code.jump_target();
  }

  fn branch_rel(&mut self) -> u32 {
    return self.regs.pc + (self.code.iconst() << 2);
  }

  // --============--
  //   Instructions
  // --============--

  fn op_add(&mut self) {
    let x = self.get_rs();
    let y = self.get_rt();
    let z = x + y;

    if overflow(x, y, z) {
      return self.enter_exception(Exception::Overflow, 0);
    }

    self.set_rd(z);
  }

  fn op_addi(&mut self) {
    let x = self.get_rs();
    let y = self.code.iconst();
    let z = x + y;

    if overflow(x, y, z) {
      return self.enter_exception(Exception::Overflow, 0);
    }

    self.set_rt(z);
  }

  fn op_addiu(&mut self) {
    let sum = self.get_rs() + self.code.iconst();
    self.set_rt(sum);
  }

  fn op_addu(&mut self) {
    let sum = self.get_rs() + self.get_rt();
    self.set_rd(sum);
  }

  fn op_and(&mut self) {
    let value = self.get_rs() & self.get_rt();
    self.set_rd(value);
  }

  fn op_andi(&mut self) {
    let value = self.get_rs() & self.code.uconst();
    self.set_rt(value);
  }

  fn op_beq(&mut self) {
    let condition = self.get_rs() == self.get_rt();
    let target = self.branch_rel();
    self.branch(target, condition);
  }

  fn op_bgtz(&mut self) {
    let condition = (self.get_rs() as i32) > 0;
    let target = self.branch_rel();
    self.branch(target, condition);
  }

  fn op_blez(&mut self) {
    let condition = (self.get_rs() as i32) <= 0;
    let target = self.branch_rel();
    self.branch(target, condition);
  }

  fn op_bne(&mut self) {
    let condition = self.get_rs() != self.get_rt();
    let target = self.branch_rel();
    self.branch(target, condition);
  }

  fn op_break(&mut self) {
    self.enter_exception(Exception::Breakpoint, 0);
  }

  fn op_bxx(&mut self) {
    // bgez rs,$nnnn
    // bgezal rs,$nnnn
    // bltz rs,$nnnn
    // bltzal rs,$nnnn
    let condition = if self.code.is_branch_gte() {
      (self.get_rs() as i32) >= 0
    } else {
      (self.get_rs() as i32) < 0
    };

    if self.code.is_branch_link() {
      self.rf.set(31, self.regs.next_pc);
    }

    let target = self.branch_rel();
    self.branch(target, condition);
  }

  fn op_cop(&mut self, n: u32) {
    if self.get_cop_usable(n) == false {
      return self.enter_exception(Exception::CopUnusable, n);
    }

    if self.code.is_cop_opc() {
      let code = self.code.cop_opc();
      return self.get_cop(n).cop_run(code);
    }

    let rd = self.code.rd();
    let rt = self.code.rt();

    match self.code.rs() {
      0 => {
        let value = self.get_cop(n).cop_read_gpr(rd);
        self.set_rt(value)
      }
      2 => {
        let value = self.get_cop(n).cop_read_ccr(rd);
        self.set_rt(value)
      }
      4 => {
        let value = self.get_register(rt);
        self.get_cop(n).cop_write_gpr(rd, value)
      }
      6 => {
        let value = self.get_register(rt);
        self.get_cop(n).cop_write_ccr(rd, value)
      }
      _ => {
        panic!("op_cop{}(0x{:08x})", n, self.get_code().raw())
      }
    }
  }

  fn op_cop0(&mut self) {
    self.op_cop(0);
  }

  fn op_cop1(&mut self) {
    self.op_cop(1);
  }

  fn op_cop2(&mut self) {
    self.op_cop(2);
  }

  fn op_cop3(&mut self) {
    self.op_cop(3);
  }

  fn op_div(&mut self) {
    let dividend = self.get_rs() as i32;
    let divisor = self.get_rt() as i32;

    if dividend == i32::MIN && divisor == -1 {
      self.regs.lo = 0x8000_0000;
      self.regs.hi = 0;
    } else if dividend >= 0 && divisor == 0 {
      self.regs.lo = 0xFFFF_FFFF as u32;
      self.regs.hi = dividend as u32;
    } else if dividend <= 0 && divisor == 0 {
      self.regs.lo = 0x0000_0001 as u32;
      self.regs.hi = dividend as u32;
    } else {
      self.regs.lo = (dividend / divisor) as u32;
      self.regs.hi = (dividend % divisor) as u32;
    }
  }

  fn op_divu(&mut self) {
    let dividend = self.get_rs();
    let divisor = self.get_rt();

    if divisor != 0 {
      self.regs.lo = dividend / divisor;
      self.regs.hi = dividend % divisor;
    } else {
      self.regs.lo = 0xFFFF_FFFF;
      self.regs.hi = dividend;
    }
  }

  fn op_j(&mut self) {
    let target = self.branch_abs();
    self.branch(target, true);
  }

  fn op_jal(&mut self) {
    self.rf.set(31, self.regs.next_pc);
    let target = self.branch_abs();
    self.branch(target, true);
  }

  fn op_jalr(&mut self) {
    let ra = self.regs.next_pc;
    let target = self.get_rs();
    self.branch(target, true);

    self.set_rd(ra);
  }

  fn op_jr(&mut self) {
    let target = self.get_rs();
    self.branch(target, true);
  }

  fn op_lb(&mut self, memory: &mut dyn IO) {
    let address = self.get_rs() + self.code.iconst();
    let data = self.read_data(memory, AddressWidth::Byte, address);
    let data = ((data & 0xFF) ^ 0x80).wrapping_sub(0x80);

    self.set_rt_load(data);
  }

  fn op_lbu(&mut self, memory: &mut dyn IO) {
    let address = self.get_rs() + self.code.iconst();
    let data = self.read_data(memory, AddressWidth::Byte, address);
    let data = data & 0xFF;

    self.set_rt_load(data);
  }

  fn op_lh(&mut self, memory: &mut dyn IO) {
    let address = self.get_rs() + self.code.iconst();
    if (address & 1) != 0 {
      return self.enter_exception(Exception::AddressErrorLoad, 0);
    }

    let data = self.read_data(memory, AddressWidth::Half, address);
    let data = ((data & 0xFFFF) ^ 0x8000).wrapping_sub(0x8000);

    self.set_rt_load(data);
  }

  fn op_lhu(&mut self, memory: &mut dyn IO) {
    let address = self.get_rs() + self.code.iconst();
    if (address & 1) != 0 {
      return self.enter_exception(Exception::AddressErrorLoad, 0);
    }

    let data = self.read_data(memory, AddressWidth::Half, address);
    let data = data & 0xFFFF;

    self.set_rt_load(data);
  }

  fn op_lui(&mut self) {
    self.set_rt(self.code.uconst() << 16);
  }

  fn op_lw(&mut self, memory: &mut dyn IO) {
    let address = self.get_rs() + self.code.iconst();
    if (address & 3) != 0 {
      return self.enter_exception(Exception::AddressErrorLoad, 0);
    }

    let data = self.read_data(memory, AddressWidth::Word, address);

    self.set_rt_load(data);
  }

  fn op_lwc(&mut self, n: u32, memory: &mut dyn IO) {
    if self.get_cop_usable(n) == false {
      return self.enter_exception(Exception::CopUnusable, n);
    }

    let address = self.get_rs() + self.code.iconst();
    if (address & 3) != 0 {
      return self.enter_exception(Exception::AddressErrorLoad, 0);
    }

    let rt = self.code.rt();
    let val = self.read_data(memory, AddressWidth::Word, address);
    self.get_cop(n).cop_write_gpr(rt, val);
  }

  fn op_lwc0(&mut self, memory: &mut dyn IO) {
    self.op_lwc(0, memory);
  }

  fn op_lwc1(&mut self, memory: &mut dyn IO) {
    self.op_lwc(1, memory);
  }

  fn op_lwc2(&mut self, memory: &mut dyn IO) {
    self.op_lwc(2, memory);
  }

  fn op_lwc3(&mut self, memory: &mut dyn IO) {
    self.op_lwc(3, memory);
  }

  fn op_lwl(&mut self, memory: &mut dyn IO) {
    let address = self.get_rs() + self.code.iconst();
    let data = self.read_data(memory, AddressWidth::Word, address & !3);
    let data = match address & 3 {
      0 => (data << 24) | (self.get_rt_forwarded() & 0x00FF_FFFF),
      1 => (data << 16) | (self.get_rt_forwarded() & 0x0000_FFFF),
      2 => (data << 8) | (self.get_rt_forwarded() & 0x0000_00FF),
      _ => (data << 0) | (self.get_rt_forwarded() & 0x0000_0000),
    };

    self.set_rt_load(data);
  }

  fn op_lwr(&mut self, memory: &mut dyn IO) {
    let address = self.get_rs() + self.code.iconst();
    let data = self.read_data(memory, AddressWidth::Word, address & !3);
    let data = match address & 3 {
      0 => (data >> 0) | (self.get_rt_forwarded() & 0x0000_0000),
      1 => (data >> 8) | (self.get_rt_forwarded() & 0xFF00_0000),
      2 => (data >> 16) | (self.get_rt_forwarded() & 0xFFFF_0000),
      _ => (data >> 24) | (self.get_rt_forwarded() & 0xFFFF_FF00),
    };

    self.set_rt_load(data);
  }

  fn op_mfhi(&mut self) {
    self.set_rd(self.regs.hi);
  }

  fn op_mflo(&mut self) {
    self.set_rd(self.regs.lo);
  }

  fn op_mthi(&mut self) {
    self.regs.hi = self.get_rs();
  }

  fn op_mtlo(&mut self) {
    self.regs.lo = self.get_rs();
  }

  fn op_mult(&mut self) {
    let rs = self.get_rs() as i32;
    let rt = self.get_rt() as i32;

    let result = (rs as i64) * (rt as i64);
    self.regs.lo = (result >> 0) as u32;
    self.regs.hi = (result >> 32) as u32;
  }

  fn op_multu(&mut self) {
    let s = self.get_rs();
    let t = self.get_rt();

    let result = (s as u64) * (t as u64);
    self.regs.lo = (result >> 0) as u32;
    self.regs.hi = (result >> 32) as u32;
  }

  fn op_nor(&mut self) {
    let value = !(self.get_rs() | self.get_rt());
    self.set_rd(value);
  }

  fn op_or(&mut self) {
    let value = self.get_rs() | self.get_rt();
    self.set_rd(value);
  }

  fn op_ori(&mut self) {
    let value = self.get_rs() | self.code.uconst();
    self.set_rt(value);
  }

  fn op_sb(&mut self, memory: &mut dyn IO) {
    let address = self.get_rs() + self.code.iconst();
    let data = self.get_rt();

    self.write_data(memory, AddressWidth::Byte, address, data);
  }

  fn op_sh(&mut self, memory: &mut dyn IO) {
    let address = self.get_rs() + self.code.iconst();
    if (address & 1) != 0 {
      return self.enter_exception(Exception::AddressErrorStore, 0);
    }

    let data = self.get_rt();
    self.write_data(memory, AddressWidth::Half, address, data);
  }

  fn op_sll(&mut self) {
    let value = self.get_rt() << self.code.sa();
    self.set_rd(value);
  }

  fn op_sllv(&mut self) {
    let value = self.get_rt() << self.get_rs();
    self.set_rd(value);
  }

  fn op_slt(&mut self) {
    let value = ((self.get_rs() as i32) < (self.get_rt() as i32)) as u32;
    self.set_rd(value);
  }

  fn op_slti(&mut self) {
    let value = ((self.get_rs() as i32) < (self.code.iconst() as i32)) as u32;
    self.set_rt(value);
  }

  fn op_sltiu(&mut self) {
    let value = (self.get_rs() < self.code.iconst()) as u32;
    self.set_rt(value);
  }

  fn op_sltu(&mut self) {
    let value = (self.get_rs() < self.get_rt()) as u32;
    self.set_rd(value);
  }

  fn op_sra(&mut self) {
    let value = ((self.get_rt() as i32) >> self.code.sa()) as u32;
    self.set_rd(value);
  }

  fn op_srav(&mut self) {
    let value = ((self.get_rt() as i32) >> self.get_rs()) as u32;
    self.set_rd(value);
  }

  fn op_srl(&mut self) {
    let value = self.get_rt() >> self.code.sa();
    self.set_rd(value);
  }

  fn op_srlv(&mut self) {
    let value = self.get_rt() >> self.get_rs();
    self.set_rd(value);
  }

  fn op_sub(&mut self) {
    let x = self.get_rs();
    let y = self.get_rt();
    let z = x - y;

    if overflow(x, !y, z) {
      return self.enter_exception(Exception::Overflow, 0);
    }

    self.set_rd(z);
  }

  fn op_subu(&mut self) {
    let value = self.get_rs() - self.get_rt();
    self.set_rd(value);
  }

  fn op_sw(&mut self, memory: &mut dyn IO) {
    let address = self.get_rs() + self.code.iconst();
    if (address & 3) != 0 {
      return self.enter_exception(Exception::AddressErrorStore, 0);
    }

    let data = self.get_rt();

    self.write_data(memory, AddressWidth::Word, address, data);
  }

  fn op_swc(&mut self, n: u32, memory: &mut dyn IO) {
    if self.get_cop_usable(n) == false {
      return self.enter_exception(Exception::CopUnusable, n);
    }

    let address = self.get_rs() + self.code.iconst();
    if (address & 3) != 0 {
      return self.enter_exception(Exception::AddressErrorStore, 0);
    }

    let rt = self.code.rt();
    let data = self.get_cop(n).cop_read_gpr(rt);
    self.write_data(memory, AddressWidth::Word, address, data);
  }

  fn op_swc0(&mut self, memory: &mut dyn IO) {
    self.op_swc(0, memory);
  }

  fn op_swc1(&mut self, memory: &mut dyn IO) {
    self.op_swc(1, memory);
  }

  fn op_swc2(&mut self, memory: &mut dyn IO) {
    self.op_swc(2, memory);
  }

  fn op_swc3(&mut self, memory: &mut dyn IO) {
    self.op_swc(3, memory);
  }

  fn op_swl(&mut self, memory: &mut dyn IO) {
    let address = self.get_rs() + self.code.iconst();
    let data = self.read_data(memory, AddressWidth::Word, address & !3);
    let data = match address & 3 {
      0 => (data & 0xFFFF_FF00) | (self.get_rt() >> 24),
      1 => (data & 0xFFFF_0000) | (self.get_rt() >> 16),
      2 => (data & 0xFF00_0000) | (self.get_rt() >> 8),
      _ => (data & 0x0000_0000) | (self.get_rt() >> 0),
    };

    self.write_data(memory, AddressWidth::Word, address & !3, data);
  }

  fn op_swr(&mut self, memory: &mut dyn IO) {
    let address = self.get_rs() + self.code.iconst();
    let data = self.read_data(memory, AddressWidth::Word, address & !3);
    let data = match address & 3 {
      0 => (data & 0x0000_0000) | (self.get_rt() << 0),
      1 => (data & 0x0000_00FF) | (self.get_rt() << 8),
      2 => (data & 0x0000_FFFF) | (self.get_rt() << 16),
      _ => (data & 0x00FF_FFFF) | (self.get_rt() << 24),
    };

    self.write_data(memory, AddressWidth::Word, address & !3, data);
  }

  fn op_syscall(&mut self) {
    self.enter_exception(Exception::SysCall, 0);
  }

  fn op_xor(&mut self) {
    let value = self.get_rs() ^ self.get_rt();
    self.set_rd(value);
  }

  fn op_xori(&mut self) {
    let value = self.get_rs() ^ self.code.uconst();
    self.set_rt(value);
  }

  fn op_und(&mut self) {
    self.enter_exception(Exception::ReservedInstruction, 0);
  }
}

fn map_address(address: u32) -> u32 {
  return address & Segment::get_mask(address);
}

fn overflow(x: u32, y: u32, z: u32) -> bool {
  (!(x ^ y) & (x ^ z) & 0x8000_0000) != 0
}
