#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub enum Segment {
  KUSeg = 0,
  KSeg0 = 1,
  KSeg1 = 2,
  KSeg2 = 3,
}

impl Segment {
  pub fn get(address: u32) -> Segment {
    SEGMENTS[(address as usize) >> 29]
  }

  pub fn get_mask(address: u32) -> u32 {
    SEGMENT_MASKS[(address as usize) >> 29]
  }

  pub fn is_cached(address: u32) -> bool {
    Self::get(address) < Segment::KSeg1
  }
}

const SEGMENTS: [Segment; 8] = [
  Segment::KUSeg,
  Segment::KUSeg,
  Segment::KUSeg,
  Segment::KUSeg,
  Segment::KSeg0,
  Segment::KSeg1,
  Segment::KSeg2,
  Segment::KSeg2,
];

const SEGMENT_MASKS: [u32; 8] = [
  0x7FFF_FFFF,
  0x7FFF_FFFF,
  0x7FFF_FFFF,
  0x7FFF_FFFF,
  0x1FFF_FFFF,
  0x1FFF_FFFF,
  0xFFFF_FFFF,
  0xFFFF_FFFF,
];

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_get_segment_works() {
    assert_eq!(Segment::KUSeg, Segment::get(0x0000_0000));
    assert_eq!(Segment::KUSeg, Segment::get(0x7FFF_FFFF));
    assert_eq!(Segment::KSeg0, Segment::get(0x8000_0000));
    assert_eq!(Segment::KSeg0, Segment::get(0x9FFF_FFFF));
    assert_eq!(Segment::KSeg1, Segment::get(0xA000_0000));
    assert_eq!(Segment::KSeg1, Segment::get(0xBFFF_FFFF));
    assert_eq!(Segment::KSeg2, Segment::get(0xC000_0000));
    assert_eq!(Segment::KSeg2, Segment::get(0xFFFF_FFFF));
  }
}
