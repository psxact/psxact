use crate::{
  memory::wram::Wram, timing::timing_add_cpu_time, util::events::EventCollector, AddressWidth, Comms, InterruptType, IO,
};

pub struct DmaChannel {
  address: u32,
  counter: u32,
  control: u32,
}

impl DmaChannel {
  pub fn new() -> DmaChannel {
    DmaChannel {
      address: 0,
      counter: 0,
      control: 0,
    }
  }
}

pub struct DmaCore {
  /// The number of channels in 'priority_lut'
  priority_len: usize,

  /// The prioritized list of channels
  priority_lut: [usize; 7],

  /// Priority control register
  pcr: u32,

  /// Interrupt control register
  icr: u32,

  /// The nominal list of channels
  channels: [DmaChannel; 7],
}

impl DmaCore {
  pub fn new() -> DmaCore {
    DmaCore {
      priority_len: 0,
      priority_lut: [0; 7],
      pcr: 0,
      icr: 0,
      channels: [
        DmaChannel::new(),
        DmaChannel::new(),
        DmaChannel::new(),
        DmaChannel::new(),
        DmaChannel::new(),
        DmaChannel::new(),
        DmaChannel::new(),
      ],
    }
  }

  fn read_fifo(&mut self, comms: &mut dyn Comms) -> u32 {
    comms.dma_read()
  }

  fn write_fifo(&mut self, comms: &mut dyn Comms, val: u32) {
    comms.dma_write(val)
  }

  fn speed(&mut self, comms: &mut dyn Comms) -> i32 {
    comms.dma_speed()
  }

  fn read_memory(&mut self, wram: &mut Wram, address: u32) -> u32 {
    wram.io_read(AddressWidth::Word, address)
  }

  fn write_memory(&mut self, wram: &mut Wram, address: u32, data: u32) {
    wram.io_write(AddressWidth::Word, address, data)
  }

  fn irq_channel(&mut self, n: usize) {
    self.channels[n].control &= !(1 << 24);

    let flag = 1 << (n + 24);
    let mask = 1 << (n + 16);

    if (self.icr & mask) != 0 {
      self.icr |= flag;
    }

    self.update_irq_active_flag();
  }

  pub fn irq_edge(&mut self, events: &mut EventCollector) {
    if (self.icr & 0x8000_0000) != 0 {
      events.set_irq(InterruptType::Dma);
    } else {
      events.ack_irq(InterruptType::Dma);
    }
  }

  fn update_irq_active_flag(&mut self) {
    let forced = ((self.icr >> 15) & 1) != 0;
    let master = ((self.icr >> 23) & 1) != 0;
    let signal = ((self.icr >> 16) & (self.icr >> 24) & 0x7F) != 0;
    let active = forced || (master && signal);

    if active {
      self.icr |= 0x8000_0000;
    } else {
      self.icr &= !0x8000_0000;
    }
  }

  fn channel_priority(&mut self, n: usize) -> u8 {
    ((self.pcr >> (4 * n)) & 15) as u8
  }

  fn set_pcr(&mut self, val: u32) {
    if self.pcr != val {
      self.pcr = val;

      self.priority_len = 0;

      for priority in 0..8 {
        for channel in (0..7).rev() {
          if self.channel_priority(channel) == (8 | priority) {
            self.priority_lut[self.priority_len] = channel;
            self.priority_len += 1;
          }
        }
      }
    }
  }

  fn set_icr(&mut self, val: u32) {
    self.icr &= 0xFF00_0000;
    self.icr |= val & 0x00FF_803F;
    self.icr &= !(val & 0x7F00_0000);
    self.update_irq_active_flag();
  }

  pub fn tick_channel_otc(&mut self, wram: &mut Wram) -> i32 {
    if (self.channels[6].control & (1 << 28)) == 0 && (self.channels[6].control & (1 << 24)) == 0 {
      return 0;
    }

    let mut address = self.channels[6].address;
    let bc = self.channels[6].counter & 0xFFFF;
    let bc = if bc == 0 { 65536 } else { bc };

    for _ in 1..bc {
      self.write_memory(wram, address, address - 4);
      address -= 4;
    }

    self.write_memory(wram, address, 0x00FF_FFFF);

    self.channels[6].control &= !(1 << 24);
    self.channels[6].control &= !(1 << 28);

    return bc as i32;
  }

  fn tick_sync_mode_0<T: Comms>(&mut self, n: usize, wram: &mut Wram, comms: &mut T) -> i32 {
    let mut address = self.channels[n].address;
    let address_step = if (self.channels[n].control & 2) != 0 { 0xFFFF_FFFC } else { 4 };

    let bc = self.channels[n].counter & 0xFFFF;
    let bc = if bc == 0 { 65536 } else { bc };

    let time = self.speed(comms) * (bc as i32);

    if (self.channels[n].control & 1) != 0 {
      for _ in 0..bc {
        let word = self.read_memory(wram, address);
        address = address.wrapping_add(address_step);

        self.write_fifo(comms, word);
      }
    } else {
      for _ in 0..bc {
        let word = self.read_fifo(comms);

        self.write_memory(wram, address, word);
        address = address.wrapping_add(address_step);
      }
    }

    return time;
  }

  fn tick_sync_mode_1<T: Comms>(&mut self, n: usize, wram: &mut Wram, comms: &mut T) -> i32 {
    let mut address = self.channels[n].address;
    let address_step = if (self.channels[n].control & 2) != 0 { 0xFFFF_FFFC } else { 4 };

    let bs = (self.channels[n].counter >> 0) & 0xFFFF;
    let bs = if bs == 0 { 65536 } else { bs };
    let ba = (self.channels[n].counter >> 16) & 0xFFFF;
    let ba = if ba == 0 { 65536 } else { ba };
    let bc = bs * ba;

    let time = self.speed(comms) * (bc as i32);

    if (self.channels[n].control & 1) != 0 {
      for _ in 0..bc {
        let word = self.read_memory(wram, address);
        address = address.wrapping_add(address_step);

        self.write_fifo(comms, word);
      }
    } else {
      for _ in 0..bc {
        let word = self.read_fifo(comms);

        self.write_memory(wram, address, word);
        address = address.wrapping_add(address_step);
      }
    }

    self.channels[n].address = address;
    self.channels[n].counter = (ba << 16) | bs;

    return time;
  }

  fn tick_sync_mode_2<T: Comms>(&mut self, n: usize, wram: &mut Wram, comms: &mut T) -> i32 {
    let mut time = 0;

    loop {
      let header = self.read_memory(wram, self.channels[n].address);
      let length = (header >> 24) & 0xFF;

      time += self.speed(comms) * ((length + 1) as i32);

      for _ in 0..length {
        self.channels[n].address += 4;

        let data = self.read_memory(wram, self.channels[n].address);
        self.write_fifo(comms, data);
      }

      self.channels[n].address = header & 0xFF_FFFF;
      if self.channels[n].address == 0xFF_FFFF {
        break;
      }
    }

    return time;
  }

  pub fn tick_channel<T: Comms>(&mut self, n: usize, wram: &mut Wram, comms: &mut T) -> i32 {
    if (self.channels[n].control & (1 << 28)) == 0 && (self.channels[n].control & (1 << 24)) == 0 {
      return 0;
    }

    // Chopping isn't supported, we'll die if a game uses it.
    assert!((self.channels[n].control & (1 << 8)) == 0, "dma with chopping enabled");

    let time = if n == 6 {
      0
    } else {
      self.channels[n].control &= !(1 << 28);

      match (self.channels[n].control >> 9) & 3 {
        0 => self.tick_sync_mode_0(n, wram, comms), // SyncMode(0): Run entire transfer
        1 => self.tick_sync_mode_1(n, wram, comms), // SyncMode(1): Wait for DRQ
        2 => self.tick_sync_mode_2(n, wram, comms), // SyncMode(2): Linked-list
        _ => {
          panic!("channel tried to use an undefined sync mode.")
        }
      }
    };

    self.irq_channel(n);

    time
  }

  fn get32(&mut self, address: u32) -> u32 {
    let channel = get_channel_index(address);
    if channel == 7 {
      match get_register_index(address) {
        0 => return self.pcr,
        1 => return self.icr,
        2 => return 0x6FFA_C611, // These values are junk, just what my
        _ => return 0x00FF_FFFF, // console happened to give last time.
      }
    } else {
      match get_register_index(address) {
        0 => return self.channels[channel].address,
        1 => return self.channels[channel].counter,
        _ => return self.channels[channel].control,
      }
    }
  }
}

impl IO for DmaCore {
  fn io_read(&mut self, width: AddressWidth, address: u32) -> u32 {
    timing_add_cpu_time(4);

    if width == AddressWidth::Byte {
      let shift = 8 * (address & 3);
      return (self.io_read(AddressWidth::Word, address) >> shift) & 0xFF;
    }

    if width == AddressWidth::Half {
      let shift = 8 * (address & 2);
      return (self.io_read(AddressWidth::Word, address) >> shift) & 0xFFFF;
    }

    let bit0 = REG_BIT0[((address / 4) & 31) as usize];
    let bit1 = REG_BIT1[((address / 4) & 31) as usize];
    return (self.get32(address) & !bit0) | bit1;
  }

  fn io_write(&mut self, width: AddressWidth, address: u32, val: u32) {
    timing_add_cpu_time(4);

    if width == AddressWidth::Byte && address == 0x1F80_10F6 {
      return self.io_write(AddressWidth::Word, 0x1F80_10F4, val << 16);
    }

    if width == AddressWidth::Word {
      let channel = get_channel_index(address);
      if channel == 7 {
        match get_register_index(address) {
          0 => return self.set_pcr(val),
          1 => return self.set_icr(val),
          _ => return,
        }
      } else {
        match get_register_index(address) {
          0 => {
            self.channels[channel].address = val & 0xFF_FFFF;
            return;
          }
          1 => {
            self.channels[channel].counter = val;
            return;
          }
          2 => {
            self.channels[channel].control = val;
            return;
          }
          _ => panic!("blah"),
        }
      }
    }

    panic!("blah");
  }
}

fn get_channel_index(address: u32) -> usize {
  ((address >> 4) & 7) as usize
}

fn get_register_index(address: u32) -> usize {
  ((address >> 2) & 3) as usize
}

const REG_BIT0: [u32; 32] = [
  0xFF00_0000,
  0x0000_0000,
  0x8E88_F8FC,
  0x8E88_F8FC, // dma0
  0xFF00_0000,
  0x0000_0000,
  0x8E88_F8FC,
  0x8E88_F8FC, // dma1
  0xFF00_0000,
  0x0000_0000,
  0x8E88_F8FC,
  0x8E88_F8FC, // dma2
  0xFF00_0000,
  0x0000_0000,
  0x8E88_F8FC,
  0x8E88_F8FC, // dma3
  0xFF00_0000,
  0x0000_0000,
  0x8E88_F8FC,
  0x8E88_F8FC, // dma4
  0xFF00_0000,
  0x0000_0000,
  0x8E88_F8FC,
  0x8E88_F8FC, // dma5
  0xFF00_0000,
  0x0000_0000,
  0xAEFF_FFFF,
  0xAEFF_FFFF, // dma6
  0x0000_0000,
  0x0000_7FC0,
  0x0000_0000,
  0x0000_0000, // ctrl
];

const REG_BIT1: [u32; 32] = [
  0x0000_0000,
  0x0000_0000,
  0x0000_0000,
  0x0000_0000, // dma0
  0x0000_0000,
  0x0000_0000,
  0x0000_0000,
  0x0000_0000, // dma1
  0x0000_0000,
  0x0000_0000,
  0x0000_0000,
  0x0000_0000, // dma2
  0x0000_0000,
  0x0000_0000,
  0x0000_0000,
  0x0000_0000, // dma3
  0x0000_0000,
  0x0000_0000,
  0x0000_0000,
  0x0000_0000, // dma4
  0x0000_0000,
  0x0000_0000,
  0x0000_0000,
  0x0000_0000, // dma5
  0x0000_0000,
  0x0000_0000,
  0x0000_0002,
  0x0000_0002, // dma6
  0x0000_0000,
  0x0000_0000,
  0x0000_0000,
  0x0000_0000, // ctrl
];
