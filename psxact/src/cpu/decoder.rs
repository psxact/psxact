#[derive(Clone, Copy)]
pub struct CpuInstr(u32);

impl CpuInstr {
  pub fn new(code: u32) -> CpuInstr {
    CpuInstr(code)
  }

  #[inline]
  pub fn raw(&self) -> u32 {
    self.0
  }

  #[inline]
  pub fn iconst(&self) -> u32 {
    ((self.0 & 0xFFFF) ^ 0x8000).wrapping_sub(0x8000)
  }

  #[inline]
  pub fn uconst(&self) -> u32 {
    self.0 & 0xFFFF
  }

  #[inline]
  pub fn sa(&self) -> u32 {
    (self.0 >> 6) & 0x1F
  }

  #[inline]
  pub fn rd(&self) -> u32 {
    (self.0 >> 11) & 0x1F
  }

  #[inline]
  pub fn rt(&self) -> u32 {
    (self.0 >> 16) & 0x1F
  }

  #[inline]
  pub fn rs(&self) -> u32 {
    (self.0 >> 21) & 0x1F
  }

  #[inline]
  pub fn is_gte(&self) -> bool {
    (self.0 & 0xFE00_0000) == 0x4A00_0000
  }

  #[inline]
  pub fn opc(&self) -> u32 {
    (self.0 >> 26) & 0x3F
  }

  #[inline]
  pub fn opc_special(&self) -> u32 {
    self.0 & 0x3F
  }

  #[inline]
  pub fn jump_target(&self) -> u32 {
    (self.0 << 2) & 0x0FFF_FFFC
  }

  #[inline]
  pub fn is_branch_link(&self) -> bool {
    (self.0 & 0x1E_0000) == 0x10_0000
  }

  #[inline]
  pub fn is_branch_gte(&self) -> bool {
    (self.0 & (1 << 16)) != 0
  }

  #[inline]
  pub fn is_cop_opc(&self) -> bool {
    (self.0 & (1 << 25)) != 0
  }

  #[inline]
  pub fn cop_opc(&self) -> u32 {
    self.0 & 0x1FF_FFFF
  }
}
