use crate::{timing::timing_add_cpu_time, AddressWidth, IO};

// These are not implemented or partially implemented.
//
// Synchronization Modes for Counter 0:
//   0 = Pause counter during Hblank(s)
//   1 = Reset counter to 0000h at Hblank(s)
//   2 = Reset counter to 0000h at Hblank(s) and pause outside of Hblank
//   3 = Pause until Hblank occurs once, then switch to Free Run
// Synchronization Modes for Counter 1:
//   Same as above, but using Vblank instead of Hblank
// Synchronization Modes for Counter 2:
//   0 or 3 = Stop counter at current value (forever, no h/v-blank start)
//   1 or 2 = Free Run (same as when Synchronization Disabled)
//
// Counter 0: 1 or 3 = Dotclock
// Counter 1: 1 or 3 = Hblank
// Counter 2: 2 or 3 = System Clock/8
//
// PSX.256-pix Dotclock = CPU_FREQ * 11 / 70
// PSX.320-pix Dotclock = CPU_FREQ * 11 / 56
// PSX.368-pix Dotclock = CPU_FREQ * 11 / 49
// PSX.512-pix Dotclock = CPU_FREQ * 11 / 35
// PSX.640-pix Dotclock = CPU_FREQ * 11 / 28

#[derive(PartialEq)]
pub enum TimerIrqFlag {
  Active = 0,
  Inactive = 1,
}

#[derive(PartialEq)]
pub enum TimerSource {
  System,
  SystemOver8,
  HBlank,
  DotClock,
}

pub enum TimerSyncMode {
  None,
  SyncMode0,
  SyncMode1,
  SyncMode2,
  SyncMode3,
}

pub struct Timer {
  counter: u16,
  control: u16,
  maximum: u16,
  running: bool,
  inhibit: bool,
}

impl Timer {
  pub fn new() -> Timer {
    Timer {
      counter: 0,
      control: 0,
      maximum: 0,
      running: false,
      inhibit: false,
    }
  }
}

pub struct TimerCore {
  timers: [Timer; 3],
  system_over_8_prescale: i32,
  in_hblank: bool,
  in_vblank: bool,
}

impl TimerCore {
  pub fn new() -> TimerCore {
    TimerCore {
      timers: [Timer::new(), Timer::new(), Timer::new()],
      system_over_8_prescale: 0,
      in_hblank: false,
      in_vblank: false,
    }
  }

  pub fn tick(&mut self, system: i32) {
    self.system_over_8_prescale += system;
    let system_over_8 = self.system_over_8_prescale / 8;
    self.system_over_8_prescale &= 7;

    if self.timer_get_source(0) == TimerSource::System {
      self.timer_run(0, system);
    }

    if self.timer_get_source(1) == TimerSource::System {
      self.timer_run(1, system);
    }

    if self.timer_get_source(2) == TimerSource::System {
      self.timer_run(2, system);
    } else if self.timer_get_source(2) == TimerSource::SystemOver8 {
      self.timer_run(2, system_over_8);
    }
  }

  pub fn enter_hblank(&mut self) {
    self.in_hblank = true;
    self.timer_blanking_sync(0, self.in_hblank);

    if self.timer_get_source(1) == TimerSource::HBlank {
      self.timer_run(1, 1);
    }
  }

  pub fn leave_hblank(&mut self) {
    self.in_hblank = false;
    self.timer_blanking_sync(0, self.in_hblank);
  }

  pub fn enter_vblank(&mut self) {
    self.in_vblank = true;
    self.timer_blanking_sync(1, self.in_vblank);
  }

  pub fn leave_vblank(&mut self) {
    self.in_vblank = false;
    self.timer_blanking_sync(1, self.in_vblank);
  }

  fn timer_run(&mut self, n: usize, mut amount: i32) {
    loop {
      let cycle_edge = 65536 - (self.timers[n].counter as i32);
      if amount >= cycle_edge {
        amount -= cycle_edge;
        self.timer_run_real(n, cycle_edge);
      } else {
        self.timer_run_real(n, amount);
        break;
      }
    }
  }

  fn timer_run_real(&mut self, n: usize, amount: i32) {
    if !self.timers[n].running || amount == 0 {
      return;
    }

    let mut counter = (self.timers[n].counter as i32) + amount;
    let mut control = self.timers[n].control as i32;
    let maximum = self.timers[n].maximum as i32;

    if counter > maximum && self.timers[n].counter <= self.timers[n].maximum {
      control |= 1 << 11;

      if (control & (1 << 3)) != 0 {
        counter %= maximum + 1;
      }

      // TODO: check for irq enable bit and issue interrupt
    }

    if counter > 0xFFFF {
      control |= 1 << 12;
      counter &= 0xFFFF;
      // TODO: check for irq enable bit and issue interrupt
    }

    self.timers[n].control = control as u16;
    self.timers[n].counter = counter as u16;
  }

  #[allow(dead_code)]
  fn timer_irq(&mut self, n: usize) {
    let repeat = (self.timers[n].control & (1 << 6)) != 0;
    let toggle = (self.timers[n].control & (1 << 7)) != 0;

    if toggle {
      if (self.timers[n].control & (1 << 10)) != 0 {
        self.timer_irq_real(n, TimerIrqFlag::Active);
      } else {
        self.timer_irq_real(n, TimerIrqFlag::Inactive);
      }
    } else {
      self.timer_irq_real(n, TimerIrqFlag::Active);

      if repeat {
        self.timer_irq_real(n, TimerIrqFlag::Inactive);
      }
    }

    self.timers[n].inhibit = !repeat;
  }

  #[allow(dead_code)]
  fn timer_irq_real(&mut self, n: usize, val: TimerIrqFlag) {
    if self.timers[n].inhibit {
      // TODO: does the IRQ flag still toggle and just IRQs are suppressed, or does bit10 stay 1?
    } else {
      if val == TimerIrqFlag::Active {
        self.timers[n].control &= !(1 << 10);
      } else {
        self.timers[n].control |= 1 << 10;
      }
    }
  }

  fn timer_get_source(&mut self, n: usize) -> TimerSource {
    match n {
      0 => {
        if (self.timers[0].control & (1 << 8)) != 0 {
          TimerSource::DotClock
        } else {
          TimerSource::System
        }
      }
      1 => {
        if (self.timers[1].control & (1 << 8)) != 0 {
          TimerSource::HBlank
        } else {
          TimerSource::System
        }
      }
      2 => {
        if (self.timers[2].control & (1 << 9)) != 0 {
          TimerSource::SystemOver8
        } else {
          TimerSource::System
        }
      }
      _ => panic!("Invalid value for parameter 'n'"),
    }
  }

  fn timer_get_sync_mode(&mut self, n: usize) -> TimerSyncMode {
    if (self.timers[n].control & (1 << 0)) == 0 {
      return TimerSyncMode::None;
    }

    match (self.timers[n].control >> 1) & 3 {
      0 => TimerSyncMode::SyncMode0,
      1 => TimerSyncMode::SyncMode1,
      2 => TimerSyncMode::SyncMode2,
      _ => TimerSyncMode::SyncMode3,
    }
  }

  fn timer_blanking_sync(&mut self, n: usize, active: bool) {
    match self.timer_get_sync_mode(n) {
      TimerSyncMode::None => (),
      TimerSyncMode::SyncMode0 => {
        // 0 = Pause counter during blank(s)
        self.timers[n].running = !active
      }
      TimerSyncMode::SyncMode1 => {
        // 1 = Reset counter to 0000h at blank(s)
        if active {
          self.timers[n].counter = 0;
        }
      }
      TimerSyncMode::SyncMode2 => {
        // 2 = Reset counter to 0000h at blank(s) and pause outside of blank
        self.timers[n].running = active;

        if active {
          self.timers[n].counter = 0;
        }
      }
      TimerSyncMode::SyncMode3 => {
        // 3 = Pause until blank occurs once, then switch to Free Run
        if active {
          self.timers[n].running = true;
        }
      }
    }
  }

  fn timer_get_counter(&mut self, n: usize) -> u16 {
    self.timers[n].counter
  }

  fn timer_get_control(&mut self, n: usize) -> u16 {
    let control = self.timers[n].control;

    self.timers[n].control &= !(1 << 11); // Reset after reading
    self.timers[n].control &= !(1 << 12); // Reset after reading

    control
  }

  fn timer_get_maximum(&mut self, n: usize) -> u16 {
    self.timers[n].maximum
  }

  fn timer_set_counter(&mut self, n: usize, val: u16) {
    self.timers[n].counter = val
  }

  fn timer_set_control(&mut self, n: usize, val: u16) {
    self.timers[n].counter = 0;
    self.timers[n].inhibit = false;
    self.timers[n].running = true;
    self.timers[n].control &= 0x1800;
    self.timers[n].control |= val & 0x3FF;
    self.timers[n].control |= 0x400;

    if (self.timers[n].control & 1) == 1 {
      let sync_mode = (self.timers[n].control >> 1) & 3;
      if n == 0 {
        // TODO: do these sync modes take effect immediately, or on the next /HBL pulse?
        match sync_mode {
          0 => self.timers[0].running = !self.in_hblank, // 0 = Pause counter during Hblank(s)
          1 => (),                                       // 1 = Reset counter to 0000h at Hblank(s)
          2 => self.timers[0].running = self.in_hblank,  // 2 = Reset counter to 0000h at Hblank(s) and pause outside of Hblank
          _ => self.timers[0].running = false,           // 3 = Pause until Hblank occurs once, then switch to Free Run
        }
      }

      if n == 1 {
        // TODO: do these sync modes take effect immediately, or on the next /VBL pulse?
        match sync_mode {
          0 => self.timers[1].running = !self.in_vblank, // 0 = Pause counter during Vblank(s)
          1 => (),                                       // 1 = Reset counter to 0000h at Vblank(s)
          2 => self.timers[1].running = self.in_vblank,  // 2 = Reset counter to 0000h at Vblank(s) and pause outside of Vblank
          _ => self.timers[1].running = false,           // 3 = Pause until Vblank occurs once, then switch to Free Run
        }
      }

      if n == 2 && (sync_mode == 0 || sync_mode == 3) {
        self.timers[n].running = false;
      }
    }
  }

  fn timer_set_maximum(&mut self, n: usize, val: u16) {
    self.timers[n].maximum = val;
  }
}

impl IO for TimerCore {
  fn io_read(&mut self, width: AddressWidth, address: u32) -> u32 {
    timing_add_cpu_time(4);

    if let AddressWidth::Byte = width {
      panic!("")
    }

    let n = ((address >> 4) & 3) as usize;

    match address & 15 {
      0 => self.timer_get_counter(n) as u32,
      4 => self.timer_get_control(n) as u32,
      8 => self.timer_get_maximum(n) as u32,
      _ => panic!(""),
    }
  }

  fn io_write(&mut self, width: AddressWidth, address: u32, data: u32) {
    timing_add_cpu_time(4);

    if let AddressWidth::Byte = width {
      panic!("")
    }

    let n = ((address >> 4) & 3) as usize;

    match address & 15 {
      0 => self.timer_set_counter(n, data as u16),
      4 => self.timer_set_control(n, data as u16),
      8 => self.timer_set_maximum(n, data as u16),
      _ => panic!(""),
    }
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_timer_0_initialized() {
    let mut timer = TimerCore::new();
    assert_eq!(0, timer.io_read(AddressWidth::Half, 0x1F80_1100));
    assert_eq!(0, timer.io_read(AddressWidth::Half, 0x1F80_1104));
    assert_eq!(0, timer.io_read(AddressWidth::Half, 0x1F80_1108));
  }

  #[test]
  fn test_timer_1_initialized() {
    let mut timer = TimerCore::new();
    assert_eq!(0, timer.io_read(AddressWidth::Half, 0x1F80_1110));
    assert_eq!(0, timer.io_read(AddressWidth::Half, 0x1F80_1114));
    assert_eq!(0, timer.io_read(AddressWidth::Half, 0x1F80_1118));
  }

  #[test]
  fn test_timer_2_initialized() {
    let mut timer = TimerCore::new();
    assert_eq!(0, timer.io_read(AddressWidth::Half, 0x1F80_1120));
    assert_eq!(0, timer.io_read(AddressWidth::Half, 0x1F80_1124));
    assert_eq!(0, timer.io_read(AddressWidth::Half, 0x1F80_1128));
  }

  #[test]
  fn test_timer_2_system_basics() {
    let mut timer = TimerCore::new();
    timer.io_write(AddressWidth::Half, 0x1F80_1120, 0);
    timer.io_write(AddressWidth::Half, 0x1F80_1124, 0);
    timer.io_write(AddressWidth::Half, 0x1F80_1128, 0);
    timer.tick(8);

    assert_eq!(8, timer.io_read(AddressWidth::Half, 0x1F80_1120));
  }

  #[test]
  fn test_timer_2_system_large_values() {
    let mut timer = TimerCore::new();
    timer.io_write(AddressWidth::Half, 0x1F80_1120, 0);
    timer.io_write(AddressWidth::Half, 0x1F80_1124, 0);
    timer.io_write(AddressWidth::Half, 0x1F80_1128, 0);
    timer.tick(65535);

    assert_eq!(65535, timer.io_read(AddressWidth::Half, 0x1F80_1120));
  }

  #[test]
  fn test_timer_2_system_over_8_basics() {
    let mut timer = TimerCore::new();
    timer.io_write(AddressWidth::Half, 0x1F80_1120, 0);
    timer.io_write(AddressWidth::Half, 0x1F80_1124, 0x200);
    timer.io_write(AddressWidth::Half, 0x1F80_1128, 0);

    timer.tick(8);
    assert_eq!(1, timer.io_read(AddressWidth::Half, 0x1F80_1120));

    timer.tick(7);
    assert_eq!(1, timer.io_read(AddressWidth::Half, 0x1F80_1120));

    timer.tick(1);
    assert_eq!(2, timer.io_read(AddressWidth::Half, 0x1F80_1120));
  }

  #[test]
  fn test_timer_2_system_over_8_large_values() {
    let mut timer = TimerCore::new();
    timer.io_write(AddressWidth::Half, 0x1F80_1120, 0);
    timer.io_write(AddressWidth::Half, 0x1F80_1124, 0x200);
    timer.io_write(AddressWidth::Half, 0x1F80_1128, 0);

    timer.tick(524_280);
    assert_eq!(0xFFFF, timer.io_read(AddressWidth::Half, 0x1F80_1120));

    timer.tick(7); // Test that remainders carry over
    assert_eq!(0xFFFF, timer.io_read(AddressWidth::Half, 0x1F80_1120));

    timer.tick(1);
    assert_eq!(0x0000, timer.io_read(AddressWidth::Half, 0x1F80_1120));
  }
}
