use crate::{timing::timing_add_cpu_time, AddressWidth, IO};

use super::MemoryBase;

pub struct Wram(MemoryBase);

impl Wram {
  pub fn new() -> Wram {
    Wram(MemoryBase::ram(WRAM_SIZE))
  }
}

impl IO for Wram {
  fn io_read(&mut self, width: AddressWidth, address: u32) -> u32 {
    timing_add_cpu_time(6);

    self.0.io_read(width, address)
  }

  fn io_write(&mut self, width: AddressWidth, address: u32, val: u32) {
    timing_add_cpu_time(6);

    self.0.io_write(width, address, val)
  }
}

pub const WRAM_SIZE: usize = 2 * 1024 * 1024;
pub const WRAM_MASK: usize = WRAM_SIZE - 1;
