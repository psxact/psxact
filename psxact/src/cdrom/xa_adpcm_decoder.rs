use super::sector::*;

pub struct XaAdpcmDecoder {
  channel: [Resampler; 2],
  prev: [[i16; 2]; 2],
}

impl XaAdpcmDecoder {
  pub fn new() -> XaAdpcmDecoder {
    XaAdpcmDecoder {
      channel: [Resampler::new(), Resampler::new()],
      prev: [[0; 2]; 2],
    }
  }

  pub fn read(&mut self) -> (i16, i16) {
    let left = self.channel[0].get_output();
    let right = self.channel[1].get_output();
    (left, right)
  }

  pub fn decode(&mut self, sector: &CdromSector) {
    for segment in 0..18 {
      self.decode_segment(sector, segment);
    }
  }

  pub fn decode_segment(&mut self, sector: &CdromSector, segment: usize) {
    let coding_info = sector.get_xa_coding_info();
    let is_stereo = ((coding_info >> 0) & 3) == 1;
    let is_18900hz = ((coding_info >> 2) & 3) == 1;
    let is_8bit = ((coding_info >> 4) & 3) == 1;

    // MAYBE_UNUSED(is_18900hz);

    assert!(!is_18900hz, "18.9khz playback isn't supported");
    assert!(!is_8bit, "8-bit playback isn't supported");

    let base = 24 + (segment * 128);
    let step = if is_stereo { 2 } else { 1 };

    let read_header = |n| -> u8 {
      return sector.get(base + n + 4);
    };
    let read_sample = |n| -> u8 {
      return sector.get(base + n + 16);
    };

    if !is_8bit {
      for stream in (0..8).step_by(step) {
        // .. day by day
        for nibble in (0..0x70).step_by(4) {
          if is_stereo {
            let header1 = read_header(stream + 0) as u8;
            let header2 = read_header(stream + 1) as u8;
            let sample = read_sample((stream / 2) + nibble) as i32;

            self.decode_sample(header1, ((sample << 12) & 0xF000) as i16, 0);
            self.decode_sample(header2, ((sample << 8) & 0xF000) as i16, 1);
          } else {
            let header = read_header(stream) as u8;
            let sample = read_sample((stream / 2) + nibble) as i32;
            let shift = if (stream & 1) != 0 { 8 } else { 12 };

            self.decode_sample(header, ((sample << shift) & 0xF000) as i16, 0);
            self.decode_sample(header, ((sample << shift) & 0xF000) as i16, 1);
          }
        }
      }
    }
  }

  pub fn decode_sample(&mut self, header: u8, sample: i16, c: usize) {
    let shift: i32 = (header & 15) as i32;
    let filter = ((header >> 4) & 3) as usize;

    let prev_left: i32 = self.prev[c][0] as i32;
    let prev_right: i32 = self.prev[c][1] as i32;

    let pos: i32 = POS_XA_ADPCM_TABLE[filter];
    let neg: i32 = NEG_XA_ADPCM_TABLE[filter];
    let mut val: i32 = (sample as i32) >> shift;
    val += ((pos * prev_left) + (neg * prev_right)) >> 6;

    if val < i16::MIN as i32 {
      val = i16::MIN as i32;
    }

    if val > i16::MAX as i32 {
      val = i16::MAX as i32;
    }

    self.prev[c][1] = self.prev[c][0];
    self.prev[c][0] = val as i16;

    self.channel[c].add_sample(val as i16);
  }
}

const POS_XA_ADPCM_TABLE: [i32; 5] = [0, 60, 115, 98, 122];
const NEG_XA_ADPCM_TABLE: [i32; 5] = [0, 0, -52, -55, -60];

pub struct Resampler {
  buffer: [i16; 32],
  offset: usize,
  count6: usize,

  output: [i16; 4096],
  output_read_index: usize,
  output_write_index: usize,
}

impl Resampler {
  pub fn new() -> Resampler {
    Resampler {
      buffer: [0; 32],
      offset: 0,
      count6: 6,
      output: [0; 4096],
      output_read_index: 0,
      output_write_index: 0,
    }
  }

  pub fn get_output(&mut self) -> i16 {
    if self.output_read_index == self.output_write_index {
      return 0;
    }

    let sample = self.output[self.output_read_index & 0xFFF];
    self.output_read_index += 1;

    return sample;
  }

  pub fn add_sample(&mut self, val: i16) {
    self.buffer[self.offset & 0x1F] = val;
    self.count6 -= 1;

    if self.count6 == 0 {
      self.count6 = 6;
      self.interpolate(0);
      self.interpolate(1);
      self.interpolate(2);
      self.interpolate(3);
      self.interpolate(4);
      self.interpolate(5);
      self.interpolate(6);
    }

    self.offset += 1;
  }

  pub fn interpolate(&mut self, phase: usize) {
    let mut sum = 0;

    for i in 0..29 {
      let val = self.buffer[(self.offset - i) & 0x1F] as i64;
      let lut = TABLE[phase][i] as i64;
      sum += (val * lut) / 0x8000;
    }

    self.add_output(sum.clamp(-32768, 32767) as i16);
  }

  pub fn add_output(&mut self, val: i16) {
    self.output[self.output_write_index & 0xFFF] = val;
    self.output_write_index += 1;
  }
}

const TABLE: [[i16; 29]; 7] = [
  [
    0x0000, 0x0000, 0x0000, 0x0000, 0x0000, -0x0002, 0x000A, -0x0022, 0x0041, -0x0054, 0x0034, 0x0009, -0x010A, 0x0400, -0x0A78,
    0x234C, 0x6794, -0x1780, 0x0BCD, -0x0623, 0x0350, -0x016D, 0x006B, 0x000A, -0x0010, 0x0011, -0x0008, 0x0003, -0x0001,
  ],
  [
    0x0000, 0x0000, 0x0000, -0x0002, 0x0000, 0x0003, -0x0013, 0x003C, -0x004B, 0x00A2, -0x00E3, 0x0132, -0x0043, -0x0267, 0x0C9D,
    0x74BB, -0x11B4, 0x09B8, -0x05BF, 0x0372, -0x01A8, 0x00A6, -0x001B, 0x0005, 0x0006, -0x0008, 0x0003, -0x0001, 0x0000,
  ],
  [
    0x0000, 0x0000, -0x0001, 0x0003, -0x0002, -0x0005, 0x001F, -0x004A, 0x00B3, -0x0192, 0x02B1, -0x039E, 0x04F8, -0x05A6,
    0x7939, -0x05A6, 0x04F8, -0x039E, 0x02B1, -0x0192, 0x00B3, -0x004A, 0x001F, -0x0005, -0x0002, 0x0003, -0x0001, 0x0000,
    0x0000,
  ],
  [
    0x0000, -0x0001, 0x0003, -0x0008, 0x0006, 0x0005, -0x001B, 0x00A6, -0x01A8, 0x0372, -0x05BF, 0x09B8, -0x11B4, 0x74BB, 0x0C9D,
    -0x0267, -0x0043, 0x0132, -0x00E3, 0x00A2, -0x004B, 0x003C, -0x0013, 0x0003, 0x0000, -0x0002, 0x0000, 0x0000, 0x0000,
  ],
  [
    -0x0001, 0x0003, -0x0008, 0x0011, -0x0010, 0x000A, 0x006B, -0x016D, 0x0350, -0x0623, 0x0BCD, -0x1780, 0x6794, 0x234C,
    -0x0A78, 0x0400, -0x010A, 0x0009, 0x0034, -0x0054, 0x0041, -0x0022, 0x000A, -0x0001, 0x0000, 0x0001, 0x0000, 0x0000, 0x0000,
  ],
  [
    0x0002, -0x0008, 0x0010, -0x0023, 0x002B, 0x001A, -0x00EB, 0x027B, -0x0548, 0x0AFA, -0x16FA, 0x53E0, 0x3C07, -0x1249, 0x080E,
    -0x0347, 0x015B, -0x0044, -0x0017, 0x0046, -0x0023, 0x0011, -0x0005, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  ],
  [
    -0x0005, 0x0011, -0x0023, 0x0046, -0x0017, -0x0044, 0x015B, -0x0347, 0x080E, -0x1249, 0x3C07, 0x53E0, -0x16FA, 0x0AFA,
    -0x0548, 0x027B, -0x00EB, 0x001A, 0x002B, -0x0023, 0x0010, -0x0008, 0x0002, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
  ],
];
