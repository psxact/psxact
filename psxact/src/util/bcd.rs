pub fn bcd_of_dec(value: u8) -> u8 {
  (((value / 10) * 16) + (value % 10)) as u8
}

pub fn bcd_to_dec(value: u8) -> u8 {
  (((value / 16) * 10) + (value % 16)) as u8
}
