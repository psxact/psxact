struct BitInfo<const BITS: usize>;

impl<const BITS: usize> BitInfo<BITS> {
  const MASK: usize = (1 << (BITS + 1)) - 1;
  const MASK_LSB: usize = (1 << BITS) - 1;
  const MASK_MSB: usize = (1 << BITS);
  const SIZE: usize = (1 << BITS);
}

pub struct Fifo<T: Copy, const BITS: usize> {
  buffer: Vec<T>,
  rd_ptr: usize,
  wr_ptr: usize,
}

impl<T: Copy + Default, const BITS: usize> Fifo<T, BITS> {
  pub fn new() -> Fifo<T, BITS> {
    let buffer = vec![T::default(); BitInfo::<BITS>::SIZE];

    Fifo {
      buffer,
      rd_ptr: 0,
      wr_ptr: 0,
    }
  }
}

impl<T: Copy, const BITS: usize> Fifo<T, BITS> {
  pub fn clear(&mut self) {
    self.rd_ptr = 0;
    self.wr_ptr = 0;
  }

  pub fn discard(&mut self, how_many: usize) {
    assert!(
      how_many < self.size(),
      "attempt to discard {}/{} items",
      how_many,
      self.size()
    );
    self.rd_ptr = (self.rd_ptr + how_many) & BitInfo::<BITS>::MASK;
  }

  pub fn at(&self, index: usize) -> T {
    assert!(index < self.size(), "attempt to read index {}/{}", index, self.size());
    return self.buffer[(self.rd_ptr + index) & BitInfo::<BITS>::MASK_LSB];
  }

  pub fn read(&mut self) -> T {
    assert!(!self.is_empty(), "attempt to read from empty fifo");
    let value = self.buffer[self.rd_ptr & BitInfo::<BITS>::MASK_LSB];
    self.rd_ptr = (self.rd_ptr + 1) & BitInfo::<BITS>::MASK;

    return value;
  }

  pub fn write(&mut self, val: T) {
    assert!(!self.is_full(), "attempt to write to full fifo");
    self.buffer[self.wr_ptr & BitInfo::<BITS>::MASK_LSB] = val;
    self.wr_ptr = (self.wr_ptr + 1) & BitInfo::<BITS>::MASK;
  }

  pub fn is_empty(&self) -> bool {
    self.rd_ptr == self.wr_ptr
  }

  pub fn is_full(&self) -> bool {
    self.rd_ptr == (self.wr_ptr ^ BitInfo::<BITS>::MASK_MSB)
  }

  pub fn size(&self) -> usize {
    self.wr_ptr.wrapping_sub(self.rd_ptr) & BitInfo::<BITS>::MASK
  }
}
