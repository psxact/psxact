use crate::InterruptType;

pub enum Event {
  IrqAck(InterruptType),
  IrqSet(InterruptType),
  HblEnter,
  HblLeave,
  VblEnter,
  VblLeave,
}

const NUM_EVENTS: usize = 10;

pub struct EventCollector(Vec<Event>);

impl EventCollector {
  #[inline]
  pub fn new() -> EventCollector {
    EventCollector(Vec::with_capacity(NUM_EVENTS))
  }

  #[inline]
  pub fn len(&self) -> usize {
    self.0.len()
  }

  #[inline]
  pub fn clear(&mut self) {
    self.0.clear()
  }

  #[inline]
  pub fn events(&self) -> &[Event] {
    &self.0
  }

  #[inline]
  pub fn ack_irq(&mut self, int: InterruptType) {
    self.0.push(Event::IrqAck(int))
  }

  #[inline]
  pub fn set_irq(&mut self, int: InterruptType) {
    self.0.push(Event::IrqSet(int))
  }

  #[inline]
  pub fn enter_hbl(&mut self) {
    self.0.push(Event::HblEnter)
  }

  #[inline]
  pub fn leave_hbl(&mut self) {
    self.0.push(Event::HblLeave)
  }

  #[inline]
  pub fn enter_vbl(&mut self) {
    self.0.push(Event::VblEnter)
  }

  #[inline]
  pub fn leave_vbl(&mut self) {
    self.0.push(Event::VblLeave)
  }
}
