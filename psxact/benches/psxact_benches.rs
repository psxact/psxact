use criterion::{black_box, criterion_group, criterion_main, BenchmarkId, Criterion};
use psxact::{
  cdrom::{
    core::{CdromCore, Disc},
    xa_adpcm_decoder::XaAdpcmDecoder,
  },
  console::{Console, InputParams},
  gpu::{core::GpuCore, types::Color},
  input::core::InputCore,
  spu::core::SpuCore,
  timer::TimerCore,
  util::events::EventCollector,
};

const BIOS_PATH: &'static str = "/mnt/bios.bin";
const DISC_PATH: &'static str = "/mnt/disc.bin";

fn cdrom_disc_tick(c: &mut Criterion) {
  let mut group = c.benchmark_group("cdrom/disc/tick");

  for input in [1, 10, 100].iter() {
    group.bench_with_input(BenchmarkId::from_parameter(input), input, |b, &amount| {
      let mut cdrom = CdromCore::new();
      let mut disc = black_box(Disc::disc(DISC_PATH).unwrap());
      let mut events = black_box(EventCollector::new());
      let mut xa = black_box(XaAdpcmDecoder::new());

      b.iter(|| cdrom.tick(amount, &mut events, &mut disc, &mut xa))
    });
  }
}

fn cdrom_no_disc_tick(c: &mut Criterion) {
  let mut group = c.benchmark_group("cdrom/no-disc/tick");

  for input in [1, 10, 100].iter() {
    group.bench_with_input(BenchmarkId::from_parameter(input), input, |b, &amount| {
      let mut cdrom = CdromCore::new();
      let mut disc = black_box(Disc::None);
      let mut events = black_box(EventCollector::new());
      let mut xa = black_box(XaAdpcmDecoder::new());

      b.iter(|| {
        cdrom.tick(amount, &mut events, &mut disc, &mut xa);
      })
    });
  }
}

fn console_run_for_one_frame(c: &mut Criterion) {
  let mut group = c.benchmark_group("console/run_for_one_frame");

  for input in [1, 2, 3].iter() {
    group.bench_with_input(BenchmarkId::from_parameter(input), input, |b, &frames| {
      let mut console = Console::new(BIOS_PATH);
      let mut disc = black_box(Disc::None);
      let input = black_box(InputParams::default());

      b.iter(|| {
        for _ in 0..frames {
          console.run_for_one_frame(&mut disc, &input);
        }
      });
    });
  }
}

fn gpu_tick(c: &mut Criterion) {
  let mut group = c.benchmark_group("gpu/tick");

  for input in &[1, 10, 100] {
    group.bench_with_input(BenchmarkId::from_parameter(input), input, |b, &amount| {
      let mut gpu = GpuCore::new();
      let mut events = black_box(EventCollector::new());

      b.iter(|| gpu.tick(amount, &mut events))
    });
  }
}

fn gpu_color_from_u16(c: &mut Criterion) {
  c.bench_function("gpu/color/from_u16", |b| {
    let val = black_box(0x7FFF);

    b.iter(|| Color::from_u16(val))
  });
}

fn input_tick(c: &mut Criterion) {
  let mut group = c.benchmark_group("input/tick");

  for input in [1, 10, 100].iter() {
    group.bench_with_input(BenchmarkId::from_parameter(input), input, |b, &amount| {
      let mut input = InputCore::new();
      let mut events = black_box(EventCollector::new());

      b.iter(|| input.tick(amount, &mut events))
    });
  }
}

fn spu_tick(c: &mut Criterion) {
  let mut group = c.benchmark_group("spu/tick");

  for input in [1, 10, 100].iter() {
    group.bench_with_input(BenchmarkId::from_parameter(input), input, |b, &amount| {
      let mut spu = SpuCore::new();
      let mut xa = black_box(XaAdpcmDecoder::new());

      b.iter(|| spu.tick(amount, &mut xa));
    });
  }
}

fn timer_tick(c: &mut Criterion) {
  let mut group = c.benchmark_group("timer/tick");

  for input in [1, 10, 100].iter() {
    group.bench_with_input(BenchmarkId::from_parameter(input), input, |b, &amount| {
      let mut timer = TimerCore::new();

      b.iter(|| timer.tick(amount));
    });
  }
}

criterion_group!(cdrom, cdrom_no_disc_tick, cdrom_disc_tick);
criterion_group!(console, console_run_for_one_frame);
criterion_group!(gpu, gpu_tick, gpu_color_from_u16);
criterion_group!(input, input_tick);
criterion_group!(spu, spu_tick);
criterion_group!(timer, timer_tick);

criterion_main!(cdrom, console, gpu, input, spu, timer);
