use sdl2::{
  audio::{AudioQueue, AudioSpecDesired},
  controller::{Button, GameController},
  event::Event,
  keyboard::Scancode,
  pixels::PixelFormatEnum,
  render::{Canvas, Texture},
  video::Window,
  EventPump, Sdl,
};

use psxact::{
  console::{InputParams, OutputParamsAudio, OutputParamsVideo},
  input::host_device::HostDevice,
};

pub struct Sdl2Audio {
  device: AudioQueue<i16>,
}

impl Sdl2Audio {
  pub fn new(sdl: &Sdl) -> Sdl2Audio {
    let audio = sdl.audio().unwrap();

    let spec = AudioSpecDesired {
      freq: Some(SAMPLE_RATE),
      channels: Some(2),
      samples: Some(SAMPLE_SIZE),
    };

    let device = audio.open_queue(None, &spec).unwrap();
    device.resume();

    Sdl2Audio { device }
  }

  pub fn render(&mut self, params: OutputParamsAudio) -> bool {
    self.device.queue(params.buffer)
  }
}

const SAMPLE_RATE: i32 = 44100;
const SAMPLE_SIZE: u16 = (SAMPLE_RATE / 60) as u16;

pub struct Sdl2Input {
  controller: GameController,
}

impl Sdl2Input {
  pub fn new(sdl: &Sdl) -> Sdl2Input {
    let input = sdl.game_controller().unwrap();
    let controller = input.open(0).unwrap();
    Sdl2Input { controller }
  }

  pub fn update(&mut self) -> InputParams {
    InputParams {
      device1: HostDevice {
        select: self.controller.button(Button::Back).into(),
        start: self.controller.button(Button::Start).into(),
        dpad_up: self.controller.button(Button::DPadUp).into(),
        dpad_right: self.controller.button(Button::DPadRight).into(),
        dpad_down: self.controller.button(Button::DPadDown).into(),
        dpad_left: self.controller.button(Button::DPadLeft).into(),
        // TODO: why doesn't the API have front and back shoulder buttons?
        left_back_shoulder: self.controller.button(Button::LeftShoulder).into(),
        right_back_shoulder: self.controller.button(Button::RightShoulder).into(),
        left_front_shoulder: self.controller.button(Button::LeftShoulder).into(),
        right_front_shoulder: self.controller.button(Button::RightShoulder).into(),
        button_3: self.controller.button(Button::Y).into(),
        button_2: self.controller.button(Button::B).into(),
        button_0: self.controller.button(Button::A).into(),
        button_1: self.controller.button(Button::X).into(),
      },
      device2: HostDevice::default(),
    }
  }
}

pub struct Sdl2Video {
  event: EventPump,
  renderer: Canvas<Window>,
  texture: Texture,
  width: u32,
  height: u32,
}

impl Sdl2Video {
  pub fn new(sdl: &Sdl) -> Sdl2Video {
    let video = sdl.video().unwrap();
    let event = sdl.event_pump().unwrap();

    sdl2::hint::set("SDL_RENDER_SCALE_QUALITY", "linear");

    let window = video
      .window("psxact", WINDOW_WIDTH, WINDOW_HEIGHT)
      .position_centered()
      .build()
      .unwrap();

    let renderer = window.into_canvas().present_vsync().accelerated().build().unwrap();

    let texture = renderer
      .create_texture_streaming(PixelFormatEnum::ARGB8888, WINDOW_WIDTH, WINDOW_HEIGHT)
      .unwrap();

    Sdl2Video {
      event,
      renderer,
      texture,
      width: WINDOW_WIDTH,
      height: WINDOW_HEIGHT,
    }
  }

  pub fn render(&mut self, params: OutputParamsVideo) -> bool {
    self.resize(params.width, params.height);

    self
      .texture
      .with_lock(None, |pixels, stride| {
        let mut dst = pixels.as_mut_ptr() as *mut u32;
        let mut src = params.buffer.as_ptr() as *const u32;

        unsafe {
          for _ in 0..params.height {
            for x in 0..params.width {
              *dst.add(x as usize) = *src.add(x as usize);
            }

            src = src.add(640);
            dst = dst.add(stride / 4);
          }
        }
      })
      .unwrap();

    self.renderer.copy(&self.texture, None, None).unwrap();
    self.renderer.present();
    self.handle_events()
  }

  fn handle_events(&mut self) -> bool {
    let mut alive = true;

    for event in self.event.poll_iter() {
      match event {
        Event::KeyDown {
          scancode: Some(Scancode::Escape),
          ..
        }
        | Event::Quit { .. } => alive = false,
        _ => continue,
      }
    }

    alive
  }

  fn resize(&mut self, w: u32, h: u32) {
    if self.width == w && self.height == h {
      return;
    }

    self.width = w;
    self.height = h;

    self.texture = self
      .renderer
      .create_texture_streaming(PixelFormatEnum::RGB888, self.width, self.height)
      .unwrap();
  }
}

const WINDOW_WIDTH: u32 = 640;
const WINDOW_HEIGHT: u32 = 480;
