mod backend;

use backend::sdl::{Sdl2Audio, Sdl2Input, Sdl2Video};

use psxact::{
  cdrom::core::Disc,
  console::{Console, InputParams},
};

use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(name = "psxact")]
pub struct Opts {
  /// Runs the emulator without input or output.
  #[structopt(short = "h", long = "headless")]
  headless: bool,

  /// The path to a disc image. Supported extensions are: '.bin'
  #[structopt(short = "d", long = "disc-path")]
  disc_path: Option<String>,

  /// The path to a BIOS file.
  bios_path: String,
}

fn main() {
  let opts = Opts::from_args();
  let mut console = Console::new(&opts.bios_path);
  let mut disc = opts.disc_path.and_then(|path| Disc::disc(&path).ok()).unwrap_or(Disc::None);

  if opts.headless {
    run_headless(&mut console, &mut disc);
  } else {
    run(&mut console, &mut disc);
  }
}

fn run(console: &mut Console, disc: &mut Disc) {
  let sdl = sdl2::init().unwrap();
  let mut input = Sdl2Input::new(&sdl);
  let mut audio = Sdl2Audio::new(&sdl);
  let mut video = Sdl2Video::new(&sdl);

  loop {
    let i = input.update();
    let o = console.run_for_one_frame(disc, &i);

    if !audio.render(o.audio) || !video.render(o.video) {
      break;
    }
  }
}

fn run_headless(console: &mut Console, disc: &mut Disc) {
  let i = InputParams::default();

  loop {
    console.run_for_one_frame(disc, &i);
  }
}
